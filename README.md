# HARRIET-NANNYONGA

## Proposed Final Project

" Farmers who operate small units in Uganda face the problem of loosing a significant amount of their output after harvest. The problem is worsened because they is no proper records kept of how much is put harvested and/or allocated to different purposes (eaten in households, sold, gifted, stored, used for replanting). Furthermore, farmers have limited access to storage or processing facilities. Much of the food spoils in the garden or as they search for a favorable market. The overall impact is to reduce the financial returns farmers earn from operating their farms." 





The proposed final project will seek to create an tool/application through which small farmers in Uganda can track their production, the different utilization of food, and through this identify and contact preferred potential storage or processing facilities to purchase their produce. The interface will be designed for farmers to use, preferrably on mobile phones. The user will be the farmers who grow a selected type of crop.  I anticipate to face challenges trying to convert the idea into a proof of concept. Among these is how to make it easy and convinient to use for people who are time constrained. The formulation of the backend processes needed to represent the farmer's preferrence could be problematic.  
