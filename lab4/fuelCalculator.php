<!DOCTYPE html>
<html lang="en"></html>

<html>

<title> Fuel Calculator </title>
<body style="background-color:powderblue;" >
<h1>Fuel Calculator to Determine Weekly Fuel Budget</h1>
<h2>The Input Values </h2>

<?php
// Declaring Variables
$AverageMilesDriven="";
$AverageMilesDrivenErr=""; // variable captures the error for average miles driven

$MilesPerGallon="";
$MilesPerGallonErr=""; // variable capturing the error for miles per gallon

$AmountSpentOnGrocery="";
$AmountSpentOnGroceryErr=""; // variable capturing the error for amount spent on grocery

$CostOfGas="";
$CostOfGasErr=""; // variable capturing the error for the cost of gas

$FuelDiscount="";

$WeeklyCostOfGas="";



if($_SERVER["REQUEST_METHOD"] =="POST"){
    if(empty($_POST["AverageMilesDriven"])){
        $AverageMilesDrivenErr = "Average Miles Driven is required";
    }else{
        $AverageMilesDriven = 
        clean_input($_POST["AverageMilesDriven"]);
        if(!is_numeric($AverageMilesDriven)){
            $AverageMilesDrivenErr = "Input must be a number";
            $AverageMilesDriven = ""; // clear invalid result for $AverageMilesDriven
         }
    }

    if(empty($_POST["MilesPerGallon"])){
        $MilesPerGallonErr = "Miles Per Gallon is required";
    }else{
        $MilesPerGallon = 
        clean_input($_POST["MilesPerGallon"]);
        if(!is_numeric($MilesPerGallon)){
            $MilesPerGallonErr = "Input must be a number";
            $MilesPerGallon = ""; // clear invalid result for $MilesPerGallon
         }
    }

    if(empty($_POST["AmountSpentOnGrocery"])){
        $AmountSpentOnGroceryErr = "Amount Spent On Grocery is required";
    }else{
        $AmountSpentOnGrocery = 
        clean_input($_POST["AmountSpentOnGrocery"]);
        if(!is_numeric($AmountSpentOnGrocery)){
            $AmountSpentOnGroceryErr = "Input must be a number";
            $AmountSpentOnGrocery = ""; // clear invalid result for $AmountSpentOnGrocery
         }
    }

    if(empty($_POST["CostOfGas"])){
        $CostOfGasErr = "Cost of gas is required";
    }else{
        $CostOfGas = 
        clean_input($_POST["CostOfGas"]);
        if(!is_numeric($CostOfGas)){
            $CostOfGasErr = "Input must be a number";
            $CostOfGas = ""; // clear invalid result for $CostOfGas
         }
    }if(!$AverageMilesDrivenErr && !$MilesPerGallonErr && !$CostOfGasErr && !$AmountSpentOnGroceryErr){
        $FuelDiscount = CalculateFuelDiscount($AmountSpentOnGrocery);
        $WeeklyCostOfGas = ComputeWeeklyGasCost($AverageMilesDriven,$MilesPerGallon, $CostOfGas, $FuelDiscount);

}


}


function clean_input($data) {
    $data = trim($data); // removes whitespace
    $data = stripslashes($data); // strips slashes
    $data = htmlspecialchars($data); //replaces htmlchars
    return $data;
}

// Calculating the Fuel Perk or Fuel Discount

function CalculateFuelDiscount($AmountSpentOnGrocery){
if($AmountSpentOnGrocery){
$FuelDiscount = (floor($AmountSpentOnGrocery / 100) * 0.10);
}else{
    $FuelDiscount = 0;
}
return $FuelDiscount;

}

// Calculating Weekly Cost of Gas

function ComputeWeeklyGasCost($AverageMilesDriven,$MilesPerGallon, $CostOfGas, $FuelDiscount){
    $WeeklyCostOfGas = ((double)$AverageMilesDriven / (double)$MilesPerGallon) * ((double)$CostOfGas - (double)$FuelDiscount);
    return ($WeeklyCostOfGas); 
}


/* return number_format($WeeklyCostOfGas, 2, "."); */
?>
<style>
    .error {color: #FF0000;}
</style>
<p><span class="error">* required field</span></p>


<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
<label for="AverageMilesDriven" >Average Miles Driven: </label>
<input type="number" name="AverageMilesDriven" id="AverageMilesDriven" required><span class="error">* <?php echo $AverageMilesDrivenErr ?> </span> <br><br>

<label for="MilesPerGallon" > Miles Driven Per Gallon: </label>
<input type="number" name="MilesPerGallon" id="MilesPerGallon" required><span class="error">* <?php echo $MilesPerGallonErr?> </span> <br><br>

<label for="AmountSpentOnGrocery" > Amount Spent On Grocery if you use fuel perks: </label>
<input type="number" name="AmountSpentOnGrocery" id="AmountSpentOnGrocery" required><span class="error">* <?php echo $AmountSpentOnGroceryErr?> </span> <br><br>

<fieldset class="form-check">
<legend>Type and Cost of Gas : <span class="error">* </span></legend>

<input class="form-check-input" id="87" type="radio" name="CostOfGas" value=1.89 required>
<label for="87">87 octane - 1.89$/gal </label><br>

<input class="form-check-input" id="87" type="radio" name="CostOfGas" value=1.99 required>
<label for="89">89 octane - 1.99$/gal </label><br>

<input class="form-check-input" id="92" type="radio" name="CostOfGas" value=2.09 required>
<label for="92">92 octane - 2.09$/gal </label><br>

</fieldset><br>

<input type="submit" value="Submit" >
</form>

<h2>The Results </h2>
<p> <?php echo "The average miles you have driven is : $AverageMilesDriven ";?> </p><br>
<p> <?php echo "The miles you have driven per gallon of gas is : $MilesPerGallon ";?> </p><br>
<p> <?php echo "The amount you have spent on grocery is : $AmountSpentOnGrocery ";?> </p><br>
<p> <?php echo "The price you are paying for gas is : $CostOfGas ";?> </p><br>
<p> <?php echo "The discount you are getting on gas is : $FuelDiscount ";?> </p><br>
<p style="font-size:20px"; style="text-align:center"; style="color:red;"><b><?php echo "The total weekly cost of gas is : $WeeklyCostOfGas ";?></b> </p><br>

</body>
</html>