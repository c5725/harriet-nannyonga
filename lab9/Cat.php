<?php
include("Animal.php");

// Inheritance. Creating a new class called Cat 

class Cat extends Animal {
    public function amountOfTimeSpentSleeping() {
        $amountOfTimeSpentSleeping = $this->age * 365 * 15;
        echo "The cat $this->name spent $amountOfTimeSpentSleeping hours sleeping.</br>";
    }
}
 
$Milo = new Cat('Milo', 4);
$Milo->amountOfTimeSpentSleeping();