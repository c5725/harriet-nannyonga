<?php
include("Animal.php");

/* This was the original Dog class before it was modified . delete it.

// Inheritance. Creating a new class called Dog 

class Dog extends Animal {
    public function convertAge() {
        $convertedAge = $this->age * 7;
        echo "The dog $this->name is $this->age years old, which is $convertedAge dog years!</br>";
    }
}
 
$pochi = new Dog('Pochi', 7);
$pochi->convertAge(); */


// Inheritance. Creating a new class called Dog; But the earlier method is removed

class Dog extends Animal {
}
 
$pochi = new Dog('Pochi', 7);


