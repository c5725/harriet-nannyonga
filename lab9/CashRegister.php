<?php
class CashRegister {
    // Properties
    protected $amountInRegister;
	// Adding the constructor 
    function __construct($amountInRegister) {
		$this->amountInRegister = $amountInRegister;
	}
   	// Adding the destructor  
	function __destruct(){
	}
	//Methods
	function set_amountInRegister($amountInRegister){
		$this-> amountInRegister = $amountInRegister;
	}
	function get_amountInRegister(){
		return $this->amountInRegister;
	}
	
    function addMoney($moneyToBeAdded) {
		$this->amountInRegister = $this->amountInRegister + $moneyToBeAdded;
		return $this->amountInRegister; 
	}

    function removeMoney($moneyToBeRemoved) {
		if($this->$moneyToBeRemoved > $this->amountInRegister){
			echo "You don't have that much money. </br>";
		} else {
			$this->amountInRegister = $this->amountInRegister - $moneyToBeRemoved;
			return $this->amountInRegister; 
		}
	}
}