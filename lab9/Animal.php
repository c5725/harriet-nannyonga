<?php
class Animal {
    // Properties
    public $name;
    public $age;
    
// inside our Animal class right after the properties
function __construct($name, $age) {
    $this->name = $name;
    $this->age = $age;
}

// inside our Animal class below the __construct method
function __destruct(){
    echo "$this->name is no longer being used, so it's getting destroyed </br>";
}

    // Methods

    function set_name($name) {
        $this->name = $name;
        }
    function get_name() {
        return $this->name;
    }

    function set_age($age) {
        $this->age = $age;
        }
    function get_age() {
        return $this->age;
    } 
    // inside of class Animal {
        function echoNameAndAge() {
            echo "$this->name is $this->age years old </br>";

        }    
}
$fido = new Animal("Fido", 7);
echo $fido->get_name() . "</br>";
echo $fido->get_age() . "</br></br>";

$fido = new Animal("Fido", 36);
echo $fido->get_name() . "</br>";
echo $fido->get_age() . "</br></br>";


$rabbit = new Animal("Sprinter", 3);
echo $rabbit->get_name() . "</br>";
echo $rabbit->get_age() . "</br></br>";


$cow = new Animal("Monster", 8);
echo $cow->get_name() . "</br>";
echo $cow->get_age() . "</br></br>";

$fido->set_name("Fido", 5);    
$fido->echoNameAndAge();



// outside our Animal class, create a new Animal object using the __construct method
$spot = new Animal('Spot', 3);
$spot->echoNameAndAge(); 

$rabbit = new Animal("Sprinter", 3);
$rabbit->echoNameAndAge();

$cow = new Animal("Monster", 8);
$cow->echoNameAndAge();

print_r($fido); 

