USE lab5;
DROP TABLE IF EXISTS movies;
CREATE TABLE movies(
primarykey INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255),
director VARCHAR(255),
releaseDate date
);

INSERT INTO movies(title, director, releaseDate)
VALUES	("Lawrence of Arabia", "David Lean", "1962-12-16"); 

INSERT INTO movies(movieID, title, director, releaseDate)
VALUES	("Troll2", "Claudio Fragasso", "1990-10-12"); 

SELECT * FROM movies;