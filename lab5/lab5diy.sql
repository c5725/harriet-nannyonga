USE Lab5;
DROP TABLE IF EXISTS vegetables;

CREATE TABLE vegetables(
primaryKey INT AUTO_INCREMENT PRIMARY KEY, 
TypeName VARCHAR(255),
Color VARCHAR(255), 
AverageHeight FLOAT,
FarmerLovesIt BOOL, 
GrowingMonths INT,
Description TEXT, 
DatePlanted DATE 
);

INSERT INTO Vegetables(TypeName, Color, AverageHeight, FarmerLovesIt, GrowingMonths,Description, DatePlanted)
VALUES("Carrot", "Orange", 2.7, TRUE, 6, "Roottuber, grows underground", "2021-10-03"),
("Tomato", "Red", 3.9, FALSE, 4, "Berry grows above ground", "2021-12-08"),
("Peas", "Green", 6.2, TRUE, 6, "Seeds in a pod", "2020-07-15");

SELECT * FROM vegetables;
