<?php
include("../cms/includes/navbar.php");
?>
<link rel="stylesheet" href="toDoList.css" >
<div class='toDoList'>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="toDo">To Do:</label>
    <input type="text" name="toDo" id="toDo"><br>
 
    <input type="checkbox" name="isDone" id="isDone" value="true">
    <label for="isDone">Is Done</label>


    <input type="submit" class="btn btn-primary" value="Submit">
  </form>
 
<?php 

$toDoItem = "";
$isDone = false;
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (clean_input($_POST['toDo'])) {
        $toDoItem = clean_input($_POST['toDo']);
    }
    if (isset($_POST['isDone'])) {
        $isDone = true;
        header('Location: http://localhost/harriet-nannyonga/lab7/toDoList.php');
    }
}


$conn = connect_to_db("toDoList");

if (!empty($toDoItem)) {
    addToDoListItem($conn, $toDoItem, $isDone);
  }
  
  if(isset($_GET['completedItemId'])) {
    completeToDoListItem($conn, $_GET['completedItemId']);
}elseif (isset($_GET['deletedItemId']))  {
    deleteToDoListItem($conn, $_GET['deletedItemId'] );
}



function addToDoListItem($conn, $item, $isDone){
    $insert = "INSERT INTO items (toDoItem,isComplete)
    VALUES (:item, :isDone)";
    $stmt = $conn->prepare($insert);
    $stmt->bindParam(':item', $item);
    $stmt->bindParam(':isDone', $isDone, PDO::PARAM_BOOL);
    $stmt->execute();
}

function completeToDoListItem($conn, $itemId) {
    $update = "UPDATE items
    SET isComplete = true
    WHERE itemId=:itemId";
$stmt = $conn->prepare($update);
$stmt->bindParam(':itemId', $itemId);
$stmt->execute();

}

function deleteToDoListItem($conn, $itemId) {
    $delete = "DELETE FROM items WHERE itemId=:itemId";
    $stmt = $conn->prepare($delete);
    $stmt->bindParam(':itemId', $itemId);
    $stmt->execute();
}








