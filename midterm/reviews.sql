DROP DATABASE midtermHarrietN IF EXISTS;

CREATE DATABASE midtermHarrietN;

USE midtermHarrietN;

DROP TABLE IF EXISTS Reviews;
CREATE TABLE Reviews(
reviewID INT AUTO_INCREMENT PRIMARY KEY,
reviewText VARCHAR(255),
numStars INT
);

INSERT INTO Reviews(reviewText, numStars)
VALUES	("Ugly chaotic spot", 1); 

INSERT INTO Reviews(reviewText, numStars)
VALUES	("Beautiful happy spot", 5); 

SELECT * FROM Reviews WHERE numStars <3;

