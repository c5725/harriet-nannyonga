<?php
include("../cms/includes/navbar.php");
$conn = connect_to_db("midtermHarrietN");
?>

<body> 


<h1> Reviewing Quality of Fruit Harvested by Farmer </h1>

<?php
// define variables and set to empty values

$reviewText = "";
$reviewTextErr = "";

$numStars = "";
$numStarsErr = "";

// Submitting Information from the form 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty($_POST["reviewText"])){
        $reviewTextErr = "Review text or comment is required";
    }else {
        $reviewText = test_input($_POST["reviewText"]);
    }
  
    if(empty($_POST["numStars"])){
        $numStarsErr= "Number of Stars or rating is required";
    }else {
        $numStars = test_input($_POST["numStars"]);
    }
}

?>

<style> 
.error {color: #FF0000;}
</style>

<p><span class="error">* denotes required field</span></p>

<div class="review"> 

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

<fieldset class="form-check">
<legend>Rating the Quality of Fruit : <span class="error">* </span></legend>

<input type="radio" id="oneStar" name="numStars" 
<?php if (isset($numStars) && $numStars=="1") echo "checked";?>
value="1" required>
<label for="1Stars">1 Stars  * </label><br>

<input type="radio" id="twoStar" name="numStars" 
<?php if (isset($numStars) && $numStars=="2") echo "checked";?>
value="2" required >
<label for="2Stars" required>2 Stars **</label><br>

<input type="radio" id="threeStar" name="numStars" 
<?php if (isset($numStars) && $numStars=="3") echo "checked";?>
value="3" required>
<label for="3Stars">3 Stars  ***</label><br>

<input type="radio" id="fourStar" name="numStars" 
<?php if (isset($numStars) && $numStars=="4") echo "checked";?>
value="4" required>
<label for="4Stars">4 Stars ****</label><br>

<input type="radio" id="fiveStar" name="numStars" 
<?php if (isset($numStars) && $numStars=="5") echo "checked";?>
value="5" required>
<label for="5Stars">5 Stars *****</label><br><br><br>
<span class="error">* <php echo $numStarsErr;?> </span><br> <br>



Review : <textarea name="reviewText" rows="8" cols="40"> <?php echo $reviewText;?> </textarea>

</fieldset><br>

<input type="submit" name="submit" value="Submit" >
</form>

</div>

<?php

// Adding the Review to the database 

$reviewText = getreviewText($conn, $_GET['reviewId']);

$numStars = getnumStars($conn, $_GET['numStars']);

if (!empty($reviewText)) {
    addNewReview($conn, $reviewText, $numStars);
  }


// Deleting the Review

if(isset(_GET['reviewId']) ) {
    deleteReview($conn, $_GET['reviewId']);

}


// Adding a Review to the Database

function addNewReview($conn, $reviewText, $numStars) {
    $addReview = "INSERT INTO Reviews(reviewText, numStars)
               VALUES (:reviewText, :numStars)";
               $stmt = $conn->prepare($addReview);
               $stmt->bindParam(':reviewText', $reviewText);
               $stmt->bindParam(':numStars', $numStars);
               $stmt->execute();
  }


// Deleting a review from the database

function deleteReview($conn, $reViewId) {
    $deleteRev = "DELETE FROM Reviews
               WHERE (reviewId=:reviewId)";
               $stmt = $conn->prepare($deleteRev);
               $stmt->bindParam(':reviewId', :$reviewId);
               $stmt->execute();
  }
  echo $review;


// Finding each item in each of the ReviewText 

function getReviewText($conn, $reviewId) {
    $selectReview = "SELECT * FROM reviews WHERE reviewId=:reviewId";
    $stmt = $conn->prepare($selectReview);
    $stmt->bindParam(':reviewId', $reviewId);
    $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach($stmt->fetchAll() as $listRow) {
    return $listRow['reviewId'];
  }
}

  /* Printing the Reviews of the Quality of Fruit. The Printing helps Checks what the information is inside the database */

  printreviewText($conn);

  function printreviewText($conn) {
      $selectReview = "SELECT * FROM Reviews";
      $stmt = $conn->prepare($selectReview);
      $stmt->execute();
   
      $stmt->setFetchMode(PDO::FETCH_ASSOC);
      foreach($stmt->fetchAll() as $listRow) {
          echo "<div class='review row'>";
          $reviewId = $listRow['reviewId'];
          $review = $listRow['reviewText'];
          $rating = $listRow['numStars'];

          echo "<p class='col-4 offset-1'>$review</p>";
          if ($rating === "1") {
              echo "<br><p class='col-2'>*</p>
              <a class='btn btn-success col-1'
              href='reviews.php?reviewId=$reviewId'>Delete</a> </br>;
          } elseif ($rating === "2") {
              echo "<br><p class='col-2'>**</p>
              <a class='btn btn-success col-1'
              href='reviews.php?reviewId=$reviewId'>Delete</a> </br>;
          } elseif ($rating === "3") {
              echo "<br><p class='col-2'>***</p>
              <a class='btn btn-success col-1'
              href='reviews.php?reviewId=$reviewId'>Delete</a> </br>;
          } elseif ($rating === "4"){
            echo "<br><p class='col-2'>****</p>
            <a class='btn btn-success col-1'
            href='reviews.php?reviewId=$reviewId'>Delete</a> </br>;

          } else ($rating === "5") {
              echo "<br><p class='col-2'>$complete</p>
              <a class='btn btn-danger offset-2 col-1' 
        href='reviews.php?reviewId=$itemId'>Delete</a> </br>";
          }        
        echo "</div>";        
      }
  }
?>
</body>
</html>