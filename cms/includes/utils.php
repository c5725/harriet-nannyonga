<?php
function clean_input($data) {
    $data = trim($data); // removes whitespace
    $data = stripslashes($data); // strips slashes
    $data = htmlspecialchars($data); // replaces html chars
    return $data;   // generates safe clean data to be used in other functions
}
 
function connect_to_db($dbName) {
    $servername = "localhost";
    $username = "root";
    $password = "";
    try {
        return new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);      
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}

function getUserId($conn, $username) {
    $select = "SELECT userId from users WHERE username=:username";
    $stmt = $conn->prepare($select);
    $stmt->bindParam(':username', $username);
    $stmt->execute();
 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
        return $listRow['userId'];
    }
}
