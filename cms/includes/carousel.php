<?php
$numSlides = 5; // the number of slides could change if prferred
 
$articles = Article::getArticlesFromDb($conn, $numSlides);
$numSlides = count($articles) < $numSlides ? count($articles) : $numSlides;
if ($numSlides > 0){
?>
    <div id="carouselExampleDark" class="carousel carousel-dark slide"               
    data-bs-ride="carousel">
        
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleDark" 
        data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 
        1"></button>
            <?php
                for ($i=1; $i<$numSlides; $i++) {
            ?>
            <button type="button" data-bs-target="#carouselExampleDark" 
            data-bs-slide-to="<?php echo $i; ?>" aria-label="Slide <?php echo $i + 1; ?>"></button>
        <?php
        } // this is closing } for our "for loop"
        ?>
        </div>

        <div class="carousel-inner">
        <?php
            foreach ($articles as $index => $article) {
        ?>
                <div class="carousel-item <?php echo ($index == 0 ? 'active"' : '"'); 
    ?> data-bs-interval="10000">
    <!-- making carousels link to the articlePage.php by using the <a> tag -->

    <a href="articlePage.php?articleId=<?php echo $article->articleId ?>">
		...
          </a>

    <!--when the article has an image, then the <img> tag is used. Else, when article does not have an image, then the <svg> tag, which was acting as a placeholder, is used -->

    <?php if (!empty($article->primaryImage)) { ?>
                <img src='data:image/jpeg;base64,<?php echo base64_encode( 
            $article->primaryImage )?>' />
              <?php
                } else {
              ?>
                <svg class="bd-placeholder-img bd-placeholder-img-lg d-block 
    w-100" width="800" height="400" xmlns="http://www.w3.org/2000/svg" role="img" 
    aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" 
    focusable="false"><title>Placeholder</title><rect width="100%" height="100%" 
    fill="#777"></rect></svg>
              <?php } ?>

                <div class="carousel-caption d-xs-block">
                    <h3><?php echo $article->title; ?></h3>
                    <p><?php echo $article->author; ?></p>
                </div>
            </div>
        <?php
        }
        ?>
   </div>

   <button class="carousel-control-prev" type="button" 
   data-bs-target="#carouselExampleDark" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" 
  data-bs-target="#carouselExampleDark" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>

  </div> 
</div>
<?php
} // this is the closing tag for our "if ($numSlides > 0){"
?>

