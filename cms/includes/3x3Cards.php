<div class="container">
  <div class="row">
    <?php
        $data = Article::getArticlesFromDb($conn, 9); // Provides information about the article
        foreach ($data as $article) {
    ?>
      <div class="col-4">

          <a class="card-wrapper"
href="../pages/articlePage.php?articleId=<?php echo $article->articleId ?>">

        <div class="card">
          <!-- displaying an image in the 3x3Cards if the article has an image (or a Blod) in the database -->
        <?php if (!empty($article->primaryImage)) { ?>
                <img src='data:image/jpeg;base64,<?php echo base64_encode( $article->primaryImage )?>' />
              <?php } ?>
            <h2><?php echo $article->title ?></h2>
            <p><?php echo $article->author ?></p>
        </div>
        </a>
      </div>
    <?php
        }
    ?>
  </div>
</div>

