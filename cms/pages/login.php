<?php
include("../includes/navbar.php");

// Checking to see if the user exists with a matching username to the one entered in the form

$name = $username = $password = "";
$passwordErr = $usernameErr = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $username = clean_input($_POST["username"]);
  $password = clean_input($_POST["password"]);
 
  if (!empty($username) && !empty($password)) {
    $usernameErr = verifyUser($username);
    $passwordErr = verifyPassword($username, $password);

    if (empty($usernameErr) && empty($passwordErr)) {
        login($username);  // checking that there are no errors & log the user in
      }
  
  }
}
// logs in the user by starting their session, saving the info of that session using superglobal & log the use into a splash page
function login($username) { // logs in the user
    // session_start(); starts the user's session. it was originally here before being put in navbar 
    $_SESSION['username'] = $username; // saves the info from session into a superglobal variable
    header("Location: splash.php"); // redirects the user into the splash page
  }
  

 // Checking that the password matches the one in the database
function verifyPassword($username, $password) {
    $conn = connect_to_db("cms");
    $selectUser = "SELECT username, userPassword FROM users WHERE username=:username";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':username', $username);
    $stmt->execute();
   
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
      $hashedPassword = $listRow['userPassword'];
    if (!password_verify($password, $hashedPassword)) {
      return "Incorrect password";
    }
    // comparing the hashed password with the password we got from user via the form
    if (!password_verify($password, $hashedPassword)) {
        return "Incorrect password";
      }
  
    }
  }
  

// Selects a user from the database so we can check if the user exits with matching user name

function verifyUser($username) {
  $conn = connect_to_db("cms");
  $selectUser = "SELECT username FROM users WHERE username=:username";
  $stmt = $conn->prepare($selectUser);
  $stmt->bindParam(':username', $username);
  $stmt->execute();
 
  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  return empty($stmt->fetchAll()) ? "Username does not exist" : "";
}
?>


<style>
    .error {color: #FF0000;}
</style>
<div class='userLoginForm container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="form-group">
                    <label for="username">Username</label>
                    <span class="error">* <?php echo $usernameErr;?></span><br>
                    <input type="text" class="form-control" name="username" id="username" required>
                </div>
                <div class="form-group">
                    <label for="password1">Password</label>
                    <span class="error">* <?php echo $passwordErr;?></span><br>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            </form>
        </div>
    </div>
</div>
