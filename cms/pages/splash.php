<?php
  include("../includes/navbar.php");
 // session_start();             starts a new session or uses the preexisting one if user is already logged in. it was originally here before being put in navbar 
 
  if (isset($_SESSION['username'])) {  // checks if session is set and if yes, it displays a simple splash page but if not he it links to login page
?>                                      
 
<div class='container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h1>Hello <?php echo $_SESSION['username'] ?></h1>
            <!-- a link which looks like a button Edit/View articles and will take user to ArticleListing.php -->
            <a class="btn btn-primary" href="ArticleListing.php">View/Edit 
Articles</a>
        </div>
    </div>
</div>
 
<?php
} else {
?>
   
<a href="login.php">Please log in to see this page</a>
 
<?php
}
 /* These were removed per lab11 instructions because it is bad practice
// remove all session variables i.e. unsets the session 
session_unset();
 
// destroy the session i.e. deletes any saved session
session_destroy(); */
?>
