SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS finalProjectHarrietNannyonga;

CREATE DATABASE finalProjectHarrietNannyonga;
use finalProjectHarrietNannyonga;

DROP TABLE IF EXISTS Farms;
CREATE TABLE Farms (
    FarmId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	FarmName VARCHAR(255) NOT NULL,
	FarmSize FLOAT(20,2) NOT NULL,
	District VARCHAR(255) NOT NULL,
	OwnedByFarmer BOOL NOT NULL,
	IsAvailableForSeason BOOL NOT NULL,
	FarmReview VARCHAR(10000),
	UserId INT,
	CropId INT,
    FOREIGN KEY (UserId) REFERENCES Users(UserId) ON DELETE CASCADE,
	FOREIGN KEY (CropId) REFERENCES Cros(CropId) ON DELETE CASCADE
);


DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
    UserId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    userName VARCHAR(255) NOT NULL, 
    userPassword VARCHAR(255) NOT NULL, 
    fullName VARCHAR(255) NOT NULL,
	PhoneNum VARCHAR(100) NOT NULL,
	Address VARCHAR(255) NOT NULL,
	EmailAdd VARCHAR(255)
);

DROP TABLE IF EXISTS Crops;
CREATE TABLE Crops (
    CropId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	CropName VARCHAR(255) NOT NULL,
	GrowthPeriod INT NOT NULL,
	IsLivestockFodderComponent BOOL NOT NULL,
	IsIndigenous BOOL NOT NULL, 
	PlantingId INT,
	HarvestAllocationId INT,
    FOREIGN KEY (PlantingId) REFERENCES Plantings(PlantingId) ON DELETE CASCADE,
	FOREIGN KEY (HarvestAllocationId) REFERENCES  HarvestAllocations(HarvestAllocationId) ON DELETE CASCADE	
);	
	

DROP TABLE IF EXISTS Plantings;
CREATE TABLE Plantings (	
    PlantingId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	QuantityPlanted FLOAT(20,2) NOT NULL,
	PlantingSeedCost FLOAT(20,2) NOT NULL,
	IsMixedCropped BOOL NOT NULL,
	AcreagePlanted FLOAT(20,2) NOT NULL,
	LaborCost FLOAT(20,2) NOT NULL,
	MachineryCost FLOAT(20,2) NOT NULL,
	FertilizerCost FLOAT(20,2) NOT NULL,
	WateringCost FLOAT(20,2) NOT NULL,
	FuelCost FLOAT(20,2) NOT NULL,
	PlantingDate DATE NOT NULL,
	Testimonial VARCHAR(300) NOT NULL,
	FarmId INT,
	FOREIGN KEY (FarmId) REFERENCES Farms(FarmId) ON DELETE CASCADE
);


DROP TABLE IF EXISTS HarvestAllocations;
CREATE TABLE HarvestAllocations (
    HarvestAllocationId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	ExpectedYield FLOAT(20,2) NOT NULL,
	ActualYield FLOAT(20,2) NOT NULL,
	LostToNature FLOAT(20,2) NOT NULL,
	DestroyedByMachine FLOAT (20,2) NOT NULL,
	LostDuringProcessing FLOAT(20,2) NOT NULL,
	ConsumedInHome FLOAT(20,2) NOT NULL,
	Pillage FLOAT(20,2) NOT NULL,
	IsForSale BOOL NOT NULL,
	MarketPriceOfHarvest FLOAT(20,2) NOT NULL,
    PlantingId INT,
    FOREIGN KEY (PlantingId) REFERENCES Plantings(PlantingId) ON DELETE CASCADE
);


/* The conneccting tables are below 


DROP TABLE IF EXISTS Farms_Crops;
CREATE TABLE Farms_Crops (
    Farm_CropId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	FarmId INT,
	CropId INT
);


DROP TABLE IF EXISTS Crops_Plantings;
CREATE TABLE Crops_Plantings (
    Crop_PlantingId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	CropId INT,
	PlantingId INT	
);	
	
    
DROP TABLE IF EXISTS Crops_HarvestAllocations;
CREATE TABLE Crops_HarvestAllocations (
    Crop_PlantingId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	CropId INT,
	HarvestAllocationId INT
);	



DROP TABLE IF EXISTS Plantings_Farms;
CREATE TABLE Plantings_Farms (	
    Planting_FarmId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	PlantingId INT,
	FarmId INT
);
	
       
DROP TABLE IF EXISTS Harvests_Plantings;
CREATE TABLE Harvests_Plantings(
    Harvest_PlantingId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    HarvestAllocationId INT,
    PlantingId INT
); */


INSERT INTO Users (userName, 	 userPassword, 		fullName, 			PhoneNum, 		Address, 								EmailAdd)
VALUES 
				("Jka4534@",      "$2y$10$tG.n6EPOYdqzuVCm7tkoD.TqbcTEvmI8heWek9nemzcUPC9wNf3YW",   "Jacob Mukasa",
        "7025640831", 	"1245 Main St, Mukono, OH 43210",     	"JMukasa@gmail.com"),
				
				
				("LE74548##",     "$2y$10$fJ9i96V3qJl1Vi4VHhMCGedzy6KIIPVAi8cHQ4194zKYz56kxCKCy",      "Eliza Lubowa",        "6149237821", 	"23 Katasy Rd, Mpigi, MI 48127",
				"LubowaE@hmca.gov"),
				
				
				("mosesk13",      "$2y$10$AS87O.q4sxN8Ntl3k7P46emBfoROgvL8N8.gQVdPDaga4k3V0tL2G",     "Night Ssemakokiro",   "7037821980", 	"68 Pwapwa St, Buikwe, CO 80301",     	"SsemaNight1@gmail.com"),
				
				
				("534youp7892@",  "$2y$10$zxrpWbnhCKJuezImsaqp3e4s9xvp9R9w8Guv6rL70f5PLiF7bnwBO",  "Jakalandina Lukodi",  "8186467005", 	"2369 Hall Dr, Masaka, CA 90712", 	    "JakaLukodi48@sping.org"),
				
				
				("523uger51",     "$2y$10$t9ZDgdokaEOg7EwY.47RY.jbQfx5F.1Q229kaX2LdYFUxYcaOC9LO",   "Herbert Okello",      "3017853462", 	"161 High St, Gulu, OH 45087",        	"JHOkello.2@munice.com"),
				
				
				("DWatu4534@#",   "$2y$10$dr2L0ibgxLGLj66yt4WUaeCI4YigtM8hmJUBefedXUbNPRMVmti3m",  "David Watuvako",      "7031256908", 	"463 Cassady Dr, Mukono, OH 43201",     "DavWatuvako@gmail.com"),
				
				
				
				("74548PaSE#N",   "$2y$10$G.NQrN5WdiqDK4YVjCIwNuIApfBi.3zRhjgqtFdGQzWhdTI7qVTM.",    "Patrick Sentongo",    "6148214905", 	"1134 Ebony Rd, Kampala, MI 48172",     "SentongoPat@fdz.gov"),
				
				
				
				("JKaso13&zi",    "$2y$10$L8oNodeti4C7D1DvfMoeNunQ.mJBwemqmrJNFj9a3wttaG32aq7PK",  "Justin Kasoozi",  	 "7038074456", 	"67 Kangaroo Ct, Mpigi, CO 80304",      "JustinKasoozi91@gmail.com"),
				
				
				("MReb892@79",    "$2y$10$cyqBW.GOzULJ6KOimg5B6.JGT3XkZgjAqoOMR0aMmYmoW/U7899na",   "Rebecca Mugalu", 	 "8187084312", 	"3624 Musical Dr, Mityana, CA 90721",   "MugaluBecca34@women.org"),
				
				
				("FM523uk735@",   "$2y$10$7omnjiqowAXFka0teUslKOdWRIZD3RY4F.rpPWO4dZy5s2eYvKRNu",  "Francis Mugerwa",     "6149067342", 	"78 Freedom Dr, Mpigi, OH 45062",       "FrankMug78@gmail.com");


INSERT INTO Farms (FarmName, 				FarmSize, 	District, OwnedByFarmer, IsAvailableForSeason, 		FarmReview, 								UserId, CropId)
VALUES	
				  ("Mukono Orchard",		2.25,   "Mukono",    TRUE,         		FALSE,  				"Not very productive",						1,			6),
				  ("Bujuta Collective",		1.50,   "Buikwe",    TRUE,         		TRUE,  					"Must not operate again", 					2,			3),
				  ("Kabale Majestic", 		4.8,    "Mpigi",     FALSE,        		TRUE,		 			"Soils neeed fertilizers", 					3,			10),
				  ("Redwood Indigo", 		13.45,  "Mityana",   TRUE,         		TRUE,				 	"Irrigation channels need an upgrade",		4,			8),
				  ("Excel Hills",			6.05,   "Mpigi",     FALSE,        		FALSE, 					"Need heavier machinery",					5,			2),
				  ("New World Mounds",		7.23,   "Gulu",      TRUE,         		FALSE,					"Acreage is too small", 					6,			4),
				  ("Bright Eclipse",		1.89,   "Buikwe",    TRUE,         		TRUE,					"Termites should be removed", 				7,			9),
				  ("Emerald Gem",			7.76,   "Mpigi",     FALSE,        		FALSE,					 "Closer to homestead",						8,			1),
				  ("Peak Cooperative",		3.67,   "Masaka",    TRUE,         		TRUE,					 "Fencing is broken, needs repair",			9,			5),
				  ("Ancestral Corner", 		10.21,  "Mpigi",     FALSE,        		TRUE,					 "Dry and hilly land", 						10,			7);
				


INSERT INTO Crops (CropName,		GrowthPeriod,	IsLivestockFodderComponent,		IsIndigenous, 	PlantingId, HarvestAllocationId)
VALUES 
				("Pinto Beans",			3,				FALSE,							TRUE,			6,			3),
				("Sweet Peas",			2,				TRUE,							FALSE,			5,			10),
				("Bell Peppers",		4,				FALSE,							FALSE,			3,			6),
				("Yellow Corn",			3,				TRUE,							TRUE,			1, 			8),
				("Groundnuts",			6,				TRUE,							TRUE,			10,			1),
				("Carrots",				5,				TRUE,							FALSE,			9,			4),
				("Red Onions",			8,				FALSE,							TRUE,			8,			7),
				("Cayenne Peppers",		4,				TRUE,							TRUE,			2,			9),
				("Green Bananas",		9,				TRUE,							TRUE,			7,			5),
				("Mangoes",				18,				FALSE,							TRUE, 			4,			2);
                
                
INSERT INTO Plantings(QuantityPlanted, PlantingSeedCost, IsMixedCropped, AcreagePlanted, LaborCost, MachineryCost, FertilizerCost, WateringCost,   FuelCost,  PlantingDate, 	Testimonial, 							FarmId)
VALUES 
						(24.13, 		18.20,				TRUE,   		4.32, 			89.23, 		80.23, 			23.12, 		42.80, 			85.92,		"2021-04-27", 	"Tough seasons require determination",      10),
						(16.05, 		13.50, 				FALSE,  		0.57, 			13.02, 		50.21, 			19.30, 		18.01, 			72.01, 		"2020-07-16", 	"Benefited a lot from the program",         6),
						(48.79, 		20.29, 				FALSE,  		6.19, 			67.56, 		41.63, 			17.62, 		20.44, 			58.07, 		"2019-11-30", 	"The cropping seasons benefited farmers",   2),
						(56.47, 		12.78, 				TRUE,   		1.81, 			42.23, 		17.02, 			31.45, 		16.08, 			99.01, 		"2021-08-13", 	"Steady as we collaborate for progress",    9),
						(82.01, 		31.12, 				TRUE,   		2.03, 			62.71, 		56.07, 			25.09, 		37.49, 			43.06, 		"2020-03-25", 	"Would recommend this useful application",  5), 
						(13.78, 		15.40,				TRUE,   		5.68, 			89.23, 		71.34, 			13.67, 		31.84, 			78.34,		"2020-09-25", 	"Poor quality seeds",     					4),
						(21.59, 		17.30, 				TRUE,  			1.92, 			13.02, 		60.15, 			20.80, 		45.32, 			89.21, 		"2018-03-06", 	"Should have planted early",         		7),
						(34.29, 		13.98, 				FALSE,  		7.49, 			67.56, 		34.63, 			14.26, 		18.43, 			67.39, 		"2017-12-15", 	"Water supply was very problematic",   		8),
						(65.74, 		 9.82, 				TRUE,   		3.61, 			42.23, 		28.42, 			21.34, 		26.35, 			45.80, 		"2020-07-23", 	"Will join the cooperative",    			1),
						(28.78, 		21.76, 				TRUE,   		5.23, 			62.71, 		67.17, 			15.19, 		17.42, 			34.31, 		"2021-06-29", 	"Training should be more regular",  		3);
                
                
      
 
INSERT INTO HarvestAllocations (ExpectedYield, ActualYield, LostToNature, DestroyedByMachine, LostDuringProcessing, ConsumedInHome, Pillage, IsForSale, MarketPriceOfHarvest, PlantingId)
VALUES
								(689.45, 		490.34, 		3.07,  			4.50,  				1.20, 			4.20,  			7.31,  	  FALSE, 			32.45,				3),
								(732.52, 		850.37,			8.90,  			2.41,  				3.62, 			3.61,  			4.42,  	  TRUE, 			26.75,				2),
								(308.69, 		400.28,			14.67, 			10.23, 				6.94, 			2.01,  			9.87,  	  TRUE, 			56.34,				7),
								(890.01, 		750.34,			82.45, 			3.21,  				4.02, 			15.47, 			60.56, 	  FALSE, 			29.50,				4),
								(530.12,  		689.67,			1.36,  			1.13,  				2.04, 			0.85,  			1.95,  	  FALSE, 			40.13,				6),
								(534.54, 		790.48, 		6.71,  			14.10,  			7.25, 			5.23,  			9.32,  	  FALSE, 			29.51,				2),
								(836.42, 		650.71,			18.94,  		12.91,  			13.26, 			8.16,  			5.12,  	  TRUE, 			16.25,				10),
								(438.67, 		560.82,			24.37, 			11.83, 				8.94, 			12.01,  		6.74,  	  TRUE, 			43.74,				9),
								(990.06, 		940.42,			40.31, 			30.51,  			14.20, 			5.76, 			10.12, 	  TRUE, 			19.32,				1),
								(570.45,  		430.71,			12.64,  		15.43,  			7.04, 			3.97,  			7.85,  	  FALSE, 			09.13,				8);


ALTER TABLE Farms ADD primaryImage MEDIUMBLOB;
ALTER TABLE Farms ADD imageTitle VARCHAR(255);

ALTER TABLE Farms ADD primaryImage2 MEDIUMBLOB;
ALTER TABLE Farms ADD imageTitle2 VARCHAR(255);

ALTER TABLE Farms ADD primaryImage3 MEDIUMBLOB;
ALTER TABLE Farms ADD imageTitle3 VARCHAR(255);

ALTER TABLE Farms ADD primaryImage4 MEDIUMBLOB;
ALTER TABLE Farms ADD imageTitle4 VARCHAR(255);

ALTER TABLE Farms ADD primaryImage5 MEDIUMBLOB;
ALTER TABLE Farms ADD imageTitle5 VARCHAR(255);

select * from Users;

select * from Farms;

select * from Crops;

select * from Plantings;

select * from HarvestAllocations;



/* select statement for  information on the All Farms */ 
SELECT Farms.*, Users.UserId, Users.fullName, Crops.CropId, Crops.CropName  
        FROM Farms 
        LEFT JOIN Users ON Users.UserId = Farms.UserId 
        LEFT JOIN Crops ON Farms.CropId = Crops.CropId;

       
   /* select statement for  information on the All Crops */      
SELECT Crops.*, Plantings.PlantingId, HarvestAllocations.HarvestAllocationId 
        FROM Crops 
        LEFT JOIN Plantings ON Plantings.PlantingId = Crops.PlantingId 
        LEFT JOIN HarvestAllocations ON Crops.HarvestAllocationId = Crops.HarvestAllocationId;


   /* select statement for  information on the All Plantings */      
SELECT Plantings.*, Farms.FarmId 
            FROM Plantings
            LEFT JOIN Farms ON Plantings.FarmId = Farms.FarmId;
 
 
    /* select statement for  information on the All HarvestAllocations */   
SELECT HarvestAllocations.*, Plantings.PlantingId 
        FROM HarvestAllocations
        LEFT JOIN Plantings ON HarvestAllocations.PlantingId = Plantings.PlantingId;