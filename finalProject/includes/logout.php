<?php
session_start(); // starts the session
session_unset(); // unsets or removes all of the session variables
session_destroy(); // destroys the session
header("Location: ../pages/login.php"); // transfers user to the login page
?>