<?php
function clean_input($data) {
    $data = trim($data); // removes whitespace
    $data = stripslashes($data); // strips slashes
    $data = htmlspecialchars($data); // replaces html chars
    return $data;
}
 
function connect_to_db($dbName) {
    $servername = "localhost";
    $username = "root"; // 
    $password = "";
    try {
        return new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);      
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}

// Logging in the user and result being recoded in the Splash page - Harriet, revist this in relation to connnect to db function above

function login($userName) {
    $_SESSION['userName'] = $userName;
    header("Location: splash.php");
  }

  // getting a new user 
  function getUserId($conn, $userName) {
    $select = "SELECT UserId from Users WHERE userName=:userName";
    $stmt = $conn->prepare($select);
    $stmt->bindParam(':userName', $userName);
    $stmt->execute();
 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
        return $listRow['UserId'];
    }
}

