<?php
include_once("Planting.php"); // Connects the Farm to the Planting. // Output is all Plantings are related to our Farm in the database. 

class Farm {

    // Parameters
    // Properties From the Farms table

    public $FarmId;
    public $FarmName;
    public $FarmSize;
    public $District;
    public $OwnedByFarmer;
	public $IsAvailableForSeason;
    public $FarmReview;
    public $UserId;
    public $CropId;
    public $primaryImage;
    public $imageTitle;


    public $AllPlantings; // brings in All the Plantings properties and methods & stored as Planting object.  Introduces the CropId
    public $AllHarvestAllocations; // brings in All the Harvest Allocation properties and methods & stored as Planting object. 
          
    // The Constructor. Contains the properties of the Farm
    
    function __construct($conn, $farmInfo) {
        $this->conn = $conn;	
        $this->FarmId                   = $farmInfo['FarmId'];
        $this->FarmName					= $farmInfo['FarmName'];
        $this->FarmSize                 = $farmInfo['FarmSize'];
        $this->District                 = $farmInfo['District'];
        $this->OwnedByFarmer            = $farmInfo['OwnedByFarmer'];
		$this->IsAvailableForSeason		= $farmInfo['IsAvailableForSeason'];
        $this->FarmReview               = $farmInfo['FarmReview'];
        $this->UserId                   = $farmInfo['UserId'];
        $this->CropId                   = $farmInfo['CropId'];
        $this->primaryImage             = $farmInfo['primaryImage'];
        $this->imageTitle               = $farmInfo['imageTitle'];
    }

    // The Destructor 
    function __destruct() {}
    
    // Extracting all information about Farms; maximum 50 Farms.  

    // user to see Farm which are both available and unAvailable For Season
    static function getFarmsFromDb($conn, $numFarms = 50, $UserId, $onlyAvailableForSeason=true) {
        if ($onlyAvailableForSeason) {
            $selectFarms = "SELECT Farms.*, Users.fullName 
            FROM Farms
            LEFT JOIN (Users) ON Users.UserId=Farms.UserId
            WHERE Farms.IsAvailableForSeason=true
            AND Farms.UserId =:UserId
            ORDER BY Farms.FarmSize DESC
            LIMIT :numFarms";
              } else {
            $selectFarms = "SELECT Farms.*, Users.fullName
            FROM Farms
            LEFT JOIN (Users) ON Users.UserId=Farms.UserId
            WHERE Farms.UserId =:UserId is not included in the lab example */
            ORDER BY Farms.FarmSize DESC
            LIMIT :numFarms";
        }                             
        $stmt = $conn->prepare($selectFarms);
        $stmt->bindParam(':numFarms', $numFarms, PDO::PARAM_INT);
        $stmt->bindParam(':UserId', $UserId, PDO::PARAM_INT);
        $stmt->execute();

        $farmList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $FarmlistRow) {
            $farm = new Farm ($conn, $FarmlistRow);
 
            $farmList[] = $farm;          
        }
        return $farmList; // list of all Farms in database and their info
    } // end of extracting all Farms from database  

// Update Farm. How do you handle the CropId which is a foreign key in the Farm table
    function updateFarm() {
        $update = "UPDATE Farms SET
            FarmName=:FarmName,
            FarmSize=:FarmSize,
            District=:District,
            OwnedByFarmer=:OwnedByFarmer,
			IsAvailableForSeason=:IsAvailableForSeason,
			FarmReview=:FarmReview,
            primaryImage=:primaryImage,
            imageTitle=:imageTitle 
            WHERE FarmId=:FarmId";
        $stmt = $this->conn->prepare($update);
        $stmt->bindParam(':FarmId', $this->FarmId, PDO::PARAM_INT);
        $stmt->bindParam(':FarmName', $this->FarmName);
	    $stmt->bindParam(':FarmSize', $this->FarmId, PDO::PARAM_FLOAT);
		$stmt->bindParam(':District', $this->District);
        $stmt->bindParam(':OwnedByFarmer', $this->OwnedByFarmer, PDO::PARAM_BOOL);
        $stmt->bindParam(':IsAvailableForSeason', $this->IsAvailableForSeason, PDO::PARAM_BOOL);
		$stmt->bindParam(':FarmReview', $this->FarmReview);
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();
    }

// Creating a new farm
    function createFarm() {
        $insert = "INSERT INTO Farms
            (FarmName, FarmSize, District, OwnedByFarmer, IsAvailableForSeason, FarmReview, UserId, primaryImage, imageTitle)
            VALUES
            (:FarmName, :FarmSize, :District, :OwnedByFarmer, :IsAvailableForSeason, :FarmReview, :UserId, :primaryImage, :imageTitle)";
        $stmt = $this->conn->prepare($insert);
        $stmt->bindParam(':FarmName', $this->FarmName);
        $stmt->bindParam(':FarmSize', $this->FarmSize);
        $stmt->bindParam(':District', $this->District);
		$stmt->bindParam(':OwnedByFarmer', $this->OwnedByFarmer, PDO::PARAM_BOOL);
        $stmt->bindParam(':IsAvailableForSeason', $this->IsAvailableForSeason, PDO::PARAM_BOOL);
        $stmt->bindParam(':FarmReview', $this->FarmReview);
        $stmt->bindParam(':UserId', $this->fullName, PDO::PARAM_INT); 
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();
    }

    // Deleting a Farm 
    function deleteFarm() {
        $delete = "DELETE FROM Farms WHERE FarmId=:FarmId";
        $stmt = $this->conn->prepare($delete);
        $stmt->bindParam(':FarmId', $this->FarmId, PDO::PARAM_INT);
        $stmt->execute();
    } // end deleting a farm

   // Extracting an Individual Farm from the Farms array. Provides the details of each farm.		

   static function getFarmById($conn, $FarmId){
        $selectFarm = "SELECT * 
        FROM Farms   
        WHERE Farms.FarmId=:FarmId";
        $stmt = $conn->prepare($selectFarm);
        $stmt->bindParam(':FarmId', $FarmId, PDO::PARAM_INT);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $FarmlistRow) {
            $farm = new Farm($conn, $FarmlistRow);
        }

        return $farm;

    } // end of extracting individual Farms from database 

// Brings in the info of All Plantings into the farm 
    function setPlanting() {
        $this->AllPlantings = Planting::getPlantingsFromDb($this->conn, 50, $this->FarmId); // brings in all the info from all Plantings and all of its variables 
    }

// Brings in the info of All Harvest Allocations into the farm 
function setHarvestAllocation() {
    foreach ($this->AllPlantings as $Planting) {
        $this->AllHarvestAllocations = HarvestAllocation::getHarvestAllocationsFromDb($this->conn, 50, $Planting->PlantingId); // brings in all the info from all HarvestAllocations and all of its variables 
    }
} 
	
} // closing the Farm Class

?>  


