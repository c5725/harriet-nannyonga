<?php
?>
<div class="container">
  <div class="row">
    <?php
    if (isset($_SESSION['userName'])){
    $UserId=getUserId($conn, $_SESSION["userName"]);
        $data = Farm::getFarmsFromDb($conn, 9, $UserId, true);
        
        foreach ($data as $farm) {
    ?>
      <d  iv class="col-12 col-md-4">
      <a class="card-wrapper"
href="../pages/fullFarm/farmPage.php?FarmId=<?php echo $farm->FarmId ?>">
        <div class="card">
          <!-- Displaying the image in the database-->
        <?php if (!empty($farm->primaryImage)) { ?>
                <img src='data:image/jpeg;base64,<?php echo base64_encode( $farm->primaryImage )?>' />
              <?php } ?>
            <h2><?php echo "Key Features of the Farm"?></h2>
            <p><?php echo "Farm Name: " . $farm->FarmName ?></p>
            <p><?php echo "Farm Size: " . $farm->FarmSize . "acres" ?></p>
			      <p> <?php echo "District Location: " .  $farm->District; ?> </p>
            <p> <?php echo "OwnedByFarmer: " .  $farm->$isOwnedByFarmer; ?> </p>
            <p> <?php echo "IsAvailableForSeason: " .  $farm->$isAvailableForSeason; ?> </p>
            <p> <?php echo "FarmReview: " .  $farm->FarmReview; ?> </p>
        </div>
        </a>
      </div>
    <?php
        }
      }
    ?>
  </div>

</div> 