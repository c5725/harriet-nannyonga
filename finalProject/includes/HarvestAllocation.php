<?php
include_once("Crop.php"); // Connects the HarvestAllocation to the Crop. // SELECT statement just SELECT * FROM HarvestAllocations WHERE PlantingId = PlantingId.  // Output is all Harvest Allocations related to Crops in the database.

class HarvestAllocation {
    // Parameters
    // Properties From the HarvestAllocations table
    public $HarvestAllocationId;
    public $ExpectedYield;
    public $ActualYield;
    public $LostToNature;
    public $DestroyedByMachine;
    public $LostDuringProcessing;
    public $ConsumedInHome;
    public $Pillage;
    public $IsForSale;
    public $MarketPriceOfHarvest;
    public $PlantingId;
    public $TotalAmountLostPostHarvest; 
    public $CropAmountAvailableForMarket;
    public $RevenueFromHarvest; 
    public $ProfitFromHarvest;
    public $TotalCostPerPlanting;

    // Methods
    // The Constructor. Contains properties from HarvestAllocations
    
    function __construct($conn, $farmInfo) {
        $this->conn = $conn;	
        $this->HarvestAllocationId      = $farmInfo['HarvestAllocationId'];
        $this->ExpectedYield            = $farmInfo['ExpectedYield'];
        $this->ActualYield              = $farmInfo['ActualYield'];
        $this->LostToNature             = $farmInfo['LostToNature'];
        $this->DestroyedByMachine       = $farmInfo['DestroyedByMachine'];
        $this->LostDuringProcessing     = $farmInfo['LostDuringProcessing'];
        $this->ConsumedInHome           = $farmInfo['ConsumedInHome'];
        $this->Pillage                  = $farmInfo['Pillage'];
        $this->IsForSale                = $farmInfo['IsForSale'];
        $this->MarketPriceOfHarvest     = $farmInfo['MarketPriceOfHarvest'];
        $this->PlantingId               = $farmInfo['PlantingId'];	
    }

    // The Destructor 
    function __destruct() {}
    
       // Extracting all HarvestAllocations and all rows in each HarvestAllocation to create an array for HarvestAllocations or a HarvestAllocationList
                
    static function getHarvestAllocationsFromDb($conn, $numHarvestAllocations = 50, $PlantingId) {
        $selectHarvestAllocations = "SELECT * 
        FROM HarvestAllocations
        WHERE HarvestAllocations.PlantingId=:PlantingId
        LIMIT :numHarvestAllocations";
        $stmt = $conn->prepare($selectHarvestAllocations);
        $stmt->bindParam(':numHarvestAllocations', $numHarvestAllocations, PDO::PARAM_INT);
        $stmt->bindParam(':PlantingId', $PlantingId, PDO::PARAM_INT);
        $stmt->execute();

        $harvestAllocationList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach($stmt->fetchAll() as $HarvestAllocationlistRow) {
                $harvestAllocation = new HarvestAllocation($conn, $HarvestAllocationlistRow);
				$harvestAllocationList[] = $harvestAllocation;
		
        }
        return $harvestAllocationList;
    }// end of extracting all HarvestAllocations from database 

		// Extracting an Individual HarvestAllocation from the HarvestAllocation array. Provides the details of each HarvestAllocation.		

    static function getHarvestAllocationById($conn, $HarvestAllocationId){
        $selectHarvestAllocation = "SELECT *
        FROM HarvestAllocations
        WHERE HarvestAllocations.HarvestAllocationId=:HarvestAllocationId";
        $stmt = $conn->prepare($selectHarvestAllocation);
        $stmt->bindParam(':HarvestAllocationId', $HarvestAllocationId, PDO::PARAM_INT);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach($stmt->fetchAll() as $HarvestAllocationlistRow) {
            $harvestAllocation = new HarvestAllocation($conn, $HarvestAllocationlistRow);
            }

        return $harvestAllocation;

    } // end of extracting individual Harvest Allocations from database 
    
	// Brings in the info of All Crops into the Harvest Allocation
	// Parameters in this crop function should include the two foreign keys in the table namely PlantingId and HarvestAllocationId
    function setCrops() {		
		$this->AllCrops = Crop::getCropsFromDb($this->conn, 50, $this->PlantingId, $this->HarvestAllocationId); // brings in all the info from all Crops and all its variables 
		// $this->AllCrops = Crop::getCropsFromDb($this->conn, 50, $this->$PlantingId, $this->$HarvestAllocationId);
    } // end of  function for including all crops into harvest allocations


    // Calculating Total Amount Lost Post Harvest 
		function calculateTotalAmountLostPostHarvest() {
			$TotalAmountLostPostHarvest = $this->LostToNature + $this->DestroyedByMachine + $this->LostDuringProcessing + $this->Pillage + $this->ConsumedInHome;
            $this->TotalAmountLostPostHarvest = $TotalAmountLostPostHarvest; // instead of returning total cost, it is saved as a property
		} // end of calculating TotalAmount loses post harvest 
	
	// Calculating the crop Amount Available For Marketing 
        function calculateCropAmountAvailableForMarket() {
            $CropAmountAvailableForMarket = $this->ActualYield - $this->TotalAmountLostPostHarvest ;
            $this->CropAmountAvailableForMarket = $CropAmountAvailableForMarket; // instead of returning total cost, it is saved as a property
        } // end of function calculating the Crop Amount Available for Market  

	 // Calculating Revenue from Harvest 
		function calculateRevenueFromHarvest() {
			$RevenueFromHarvest = ( ($this->ActualYield - ($this->LostToNature + $this->DestroyedByMachine + $this->LostDuringProcessing + $this->Pillage + $this->ConsumedInHome)) * $this->MarketPriceOfHarvest);
            $this->RevenueFromHarvest = $RevenueFromHarvest; // instead of returning total cost, it is saved as a property
		} // end of function calculating the Revenue from Harvest


 // Calculating Profit from Harvest -- trial 2 - removes error message but value is wrong 
   function calculateProfitFromHarvest() {
    $ProfitFromHarvest = $this->RevenueFromHarvest  - $this->TotalCostPerPlanting;
    $this->ProfitFromHarvest = $ProfitFromHarvest; // instead of returning total cost, it is saved as a property
} // end of function calculating Profit from Harvest

} // Ending the Harvest Allocation Class

?>  