<?php
$numSlides = 5;

$farms = Farm::getFarmsFromDb($conn, $numSlides, $UserId); // added UserId?
$numSlides = count($farms) < $numSlides ? count($farms) : $numSlides;
if ($numSlides > 0) {
    ?>
     
        <div id="carouselExampleDark" class="carousel carousel-dark slide"               
    data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleDark" 
                data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 
                1"></button>
                <?php
                for ($i=1; $i<$numSlides; $i++) {
            
                ?>
                     <button type="button" data-bs-target="#carouselExampleDark" 
                    data-bs-slide-to="<?php echo $i; ?>" aria-label="Slide <?php echo $i + 1; 
                     ?>"></button>
                  
                    <?php
      
                    }
                     ?>

            </div>   
            <div class="carousel-inner">
      <?php
        foreach ($farms as $index => $farm) {
      ?>
            <div class="carousel-item <?php echo ($index == 0 ? 'active"' : '"'); 
            ?> data-bs-interval="10000">
          <a href="farmPage.php?FarmId=<?php echo $farm->FarmId ?>">
            <!-- displaying the images in the carousel. svg is placeholder for farms which have no images -->
              <?php if (!empty($farm->primaryImage)) { ?>
                              <img src='data:image/jpeg;base64,<?php echo base64_encode( $farm->primaryImage )?>' />
                            <?php
                              } else {
                            ?>
                              <svg class="bd-placeholder-img bd-placeholder-img-lg d-block w-100" 
                        width="800" height="400" xmlns="http://www.w3.org/2000/svg" role="img" 
                        aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" 
                        focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"></rect></svg>
                            <?php } ?>

                <div class="carousel-caption d-xs-block">
                    <h3><?php echo $farm->FarmName; ?></h3>
                    <p><?php echo $farm->FarmSize; ?></p>
                </div> <!--closing div for class="carousel-item" -->
                </a>
            </div>
        <?php
        }
        ?>
  </div> 

    <button class="carousel-control-prev" type="button" 
  data-bs-target="#carouselExampleDark" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" 
  data-bs-target="#carouselExampleDark" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>

        </div> 
            <?php
    }
    
?>