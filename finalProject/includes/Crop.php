<?php
class Crop {

    // Parameters
    // Properties From the Crops table

    public $CropId; 
    public $CropName;
    public $GrowthPeriod;
    public $IsLivestockFodderComponent;
    public $IsIndigenous;
	public $PlantingId;
	public $HarvestAllocationId; 

    // Methods
    // The Constructor. Brings the properties from Crops table
    
    function __construct($conn, $farmInfo) {
        $this->conn = $conn;	
        $this->CropId                   		= $farmInfo['CropId'];
        $this->CropName                 		= $farmInfo['CropName'];
        $this->GrowthPeriod                     = $farmInfo['GrowthPeriod'];
        $this->IsLivestockFodderComponent       = $farmInfo['IsLivestockFodderComponent'];
        $this->IsIndigenous                     = $farmInfo['IsIndigenous'];
		$this->PlantingId               		= $farmInfo['PlantingId'];
		$this->HarvestAllocationId              = $farmInfo['HarvestAllocationId'];
    } // ends the Constructor
	
    // The Destructor 
    function __destruct() {}
    
    // Extracting all Crops and all rows in each Crop to create an array for Crops or a CropList
                
    static function getCropsFromDb($conn, $numCrops = 50, $PlantingId, $HarvestAllocationId) {
        $selectCrops = "SELECT * 
        FROM Crops
        WHERE Crops.PlantingId=:PlantingId  
        AND Crops.HarvestAllocationId=:HarvestAllocationId
        LIMIT :numCrops"; 
        $stmt = $conn->prepare($selectCrops);
        $stmt->bindParam(':numCrops', $numCrops, PDO::PARAM_INT);
        $stmt->bindParam(':PlantingId', $PlantingId, PDO::PARAM_INT);
        $stmt->bindParam(':HarvestAllocationId', $HarvestAllocationId, PDO::PARAM_INT);
        $stmt->execute();

        $cropList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach($stmt->fetchAll() as $CroplistRow) {
                $crop = new Crop($conn, $CroplistRow);
				$cropList[] = $crop;
        }
        return $cropList;
    } // end of method extracting all Crops from database 


    // Extracting an Individual Crop from the Crops array. Provides the details of each crop.		
    static function getCropById($conn, $CropId){
        $selectCrop = "SELECT * 
        FROM Crops 
        WHERE CropId=:CropId";
        $stmt = $conn->prepare($selectCrop);
        $stmt->bindParam(':CropId', $CropId, PDO::PARAM_INT);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $CroplistRow) {
            $crop = new Crop($conn, $CroplistRow);
        }   
        return $crop;

    } // end of method extracting individual crops from database 

} // End of the Crop Class