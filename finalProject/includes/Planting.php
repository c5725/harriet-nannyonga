<?php 
include_once("HarvestAllocation.php"); // Connects the Planting to the HarvestAllocation. // Note that the SELECT statement of Planting, you just SELECT * FROM Plantings WHERE FarmId = FarmId.  // Output is that all Plantings are related to our Harvest Allocations in the database.
 
class Planting {
    // Parameters
    // Properties From the Plantings table

    public $PlantingId;
    public $QuantityPlanted;
    public $PlantingSeedCost;
    public $IsMixedCropped;
    public $AcreagePlanted;
    public $LaborCost;
    public $MachineryCost; 
    public $FertilizerCost;
    public $WateringCost;
    public $FuelCost;
    public $PlantingDate;
    public $Testimonial; 
	public $FarmId; 
    public $TotalCostPerPlanting;
    public $CropName;
	
    // Methods  
 // The Constructor. The properties bring in the Plantings

 function __construct($conn, $farmInfo) {
    $this->conn = $conn;	
    $this->PlantingId               = $farmInfo['PlantingId']; 
    $this->QuantityPlanted          = $farmInfo['QuantityPlanted'];
    $this->PlantingSeedCost         = $farmInfo['PlantingSeedCost'];
    $this->IsMixedCropped           = $farmInfo['IsMixedCropped'];
    $this->AcreagePlanted           = $farmInfo['AcreagePlanted'];
    $this->LaborCost                = $farmInfo['LaborCost'];
    $this->MachineryCost            = $farmInfo['MachineryCost'];
    $this->FertilizerCost           = $farmInfo['FertilizerCost'];
    $this->WateringCost             = $farmInfo['WateringCost'];
    $this->FuelCost                 = $farmInfo['FuelCost'];
    $this->PlantingDate             = $farmInfo['PlantingDate'];
    $this->Testimonial              = $farmInfo['Testimonial'];	
    $this->FarmId                   = $farmInfo['FarmId'];	
} // ends the constructor

   // The Destructor 
   function __destruct() {}

    //  Extract all Plantings and all rows in each Planting from database. Creates an array for Plantings or a PlantingList
    static function getPlantingsFromDb($conn, $numPlantings = 50, $FarmId) {
        $selectPlantings = "SELECT * 
        FROM Plantings
        WHERE Plantings.FarmId=:FarmId
        LIMIT :numPlantings";
        $stmt = $conn->prepare($selectPlantings);
        $stmt->bindParam(':numPlantings', $numPlantings, PDO::PARAM_INT); 
        $stmt->bindParam(':FarmId', $FarmId, PDO::PARAM_INT);
        $stmt->execute();

        $plantingList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach($stmt->fetchAll() as $PlantinglistRow) {
                $planting = new Planting($conn, $PlantinglistRow);
                $plantingList[] = $planting;
                
        }
        return $plantingList;
    } // end of method extracting all Plantings from database 

   // Extracting an Individual Plantings from the Plantings array. Provides the details of each planting.
    static function getPlantingById($conn, $PlantingId) {
        $selectPlantings = "SELECT * 
        FROM Plantings
        WHERE Plantings.PlantingId=:PlantingId";
		$stmt = $conn->prepare($selectPlantings);
        $stmt->bindParam(':PlantingId', $PlantingId, PDO::PARAM_INT);
        $stmt->execute();
   
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        foreach($stmt->fetchAll() as $PlantinglistRow) {
            $planting = new Planting($conn, $PlantinglistRow);
        }
            
        return $planting;
    }   // end of method extracting individual Plantings from database 

	// Brings in the info of All Harvest Allocations into the Planting
    function setHarvestAllocations() {		
		$this->AllHarvestAllocations    = HarvestAllocation::getHarvestAllocationsFromDb($this->conn, 50, $this->PlantingId); // brings in all the info from all Harvest Allocations and all of its variables 
    }

    // Calculating total cost of planting on a farm for this growth period
    function calculateTotalCostPerPlanting() {
        $TotalCostPerPlanting = ($this->PlantingSeedCost + $this->LaborCost + $this->MachineryCost + $this->FertilizerCost + $this->WateringCost + $this->FuelCost) ;
        $this->TotalCostPerPlanting = $TotalCostPerPlanting; // instead of returning total cost, it is saved as a property
    } // end of Calculation of total cost per planting
    
}// ends the Plantings Class

?>