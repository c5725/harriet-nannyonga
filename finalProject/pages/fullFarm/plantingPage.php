<?php
include("../../includes/navbar.php");
include("../../includes/Planting.php"); // Brings in the Planting Class

$planting = Planting::getPlantingById($conn, $_GET['PlantingId']);
$planting->calculateTotalCostPerPlanting();

// Convert the variables whose data type is Bool back to rendering true or false instead of 1, 0  
$isMixedCropped = '';
  if ($planting->IsMixedCropped){
    $isMixedCropped ='Yes';
  } else {
    $isMixedCropped = 'No';
  }
?>
 
<header class="masthead">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="post-heading">
                      <h1><?php echo "Planting Season Details  "; ?></h1>
                    <span class="meta">
						<p> <?php echo "Acreage Planted: " .  $planting->AcreagePlanted; ?> </p>
                        <p> <?php echo "Total Cost Per Planting: " .  $planting->TotalCostPerPlanting; ?> </p>
						<p> <?php echo "Testimonial: " .  $planting->Testimonial; ?> </p>
                    </span>
                </div>
            </div>
        </div>
    </div>
</header>
 
<planting class="mb-4">
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
				<h2><?php echo "Planting Decisions on Farm"; ?></h2>
						<p> <?php echo "Quantity Planted: " .  $planting->QuantityPlanted; ?> </p>
						<p><?php echo "Mixed Cropping System: " .  $isMixedCropped; ?> </p>  <!-- Boolean is converted back to Yes or No -->
						<p> <?php echo "Planting Date: " .  $planting->PlantingDate; ?> </p>					
		   				<p> <?php echo "Planting Seed Cost: " .  $planting->PlantingSeedCost; ?> </p>					
		   				<p> <?php echo "Labor Cost: " .  $planting->LaborCost; ?> </p>	   
						<p> <?php echo "Machinery Cost: " .  $planting->MachineryCost; ?> </p>	
		   				<p> <?php echo "Fertilizer Cost: " .  $planting->FertilizerCost; ?> </p>		   
		   				<p> <?php echo "Watering Cost: " .  $planting->WateringCost; ?> </p>	   
		   				<p> <?php echo "Fuel Cost: " .  $planting->FuelCost; ?> </p>		   
            </div>
        </div>
    </div>
</planting>