<?php
include("../../includes/navbar.php"); 
include("../../includes/HarvestAllocation.php"); 

// linking harvest Allocations to Crops

$harvestAllocation = HarvestAllocation::getHarvestAllocationById($conn, $_GET['HarvestAllocationId']);  
$harvestAllocation->setCrops();  // put set function immediately below

// Convert the variables whose data type is Bool back to rendering true or false instead of 1, 0  
$isForSale = '';
  if ($harvestAllocation->IsForSale){
    $isForSale ='Yes';
  } else {
    $isForSale = 'No';
  }
?>
 
<header class="masthead">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="post-heading">
                    <h1><?php echo "Details of Allocations of Harvest and Earnings on Farm" ;?></h1>
                    <span class="meta">	
							<p> <?php echo "Actual Yield: " .  $harvestAllocation->ActualYield; ?> </p>
							<p> <?php echo "Market Price Of Harvest: " .  $harvestAllocation->MarketPriceOfHarvest; ?> </p>			
							<p> <?php echo " Total Amount Lost Post Harvest: " . $harvestAllocation->calculateTotalAmountLostPostHarvest(); ?> </p>				
							<p> <?php echo "Revenue from Harvest: " .  $harvestAllocation->calculateRevenueFromHarvest(); ?> </p>
							<p> <?php echo "Profit from Harvest: " .  $harvestAllocation->calculateProfitFromHarvest(); ?> </p>          
					</span>
                </div>
            </div>
        </div>
    </div>
</header>   
 
<harvestAllocation class="mb-4">
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
		    <h2><?php echo "Harvest Allocations on Farm "; ?></h2>	
						<p> <?php echo "Expected Yield: " .  $harvestAllocation->ExpectedYield; ?> </p>
						<p> <?php echo "Actual Yield: " .  $harvestAllocation->ActualYield;; ?> </p>
						<p> <?php echo "Is Harvest For Sale?: " .  $isForSale; ?> </p> <!-- Boolean converted back to Yes or NO -->
                        <p> <?php echo "Market Price Of Harvest: " .  $harvestAllocation->MarketPriceOfHarvest; ?> </p>	
						<p> <?php echo "Harvest Lost To Nature : " .  $harvestAllocation->LostToNature; ?> </p>
						<p> <?php echo "Harvest Destroyed By Machinery: " .  $harvestAllocation->DestroyedByMachine; ?> </p>
						<p> <?php echo "Harvest Lost During Processing: " .  $harvestAllocation->LostDuringProcessing; ?> </p>		
						<p> <?php echo "Harvest Consumed In Home: " .  $harvestAllocation->ConsumedInHome; ?> </p>
						<p> <?php echo "Harvest Lost To Pillage: " .  $harvestAllocation->Pillage; ?> </p>	
                        <p> <?php echo "Total Amount Lost Post Harvest: " .  $harvestAllocation->TotalAmountLostPostHarvest; ?> </p>
						<p> <?php echo "Crop Amount Available For Market: " . $harvestAllocation->CropAmountAvailableForMarket; ?> </p>
						<p> <?php echo "Revenue from Harvest: " .  $harvestAllocation->RevenueFromHarvest; ?> </p>
                       <p> <?php echo "Profit from Harvest: " .  $harvestAllocation->ProfitFromHarvest; ?> </p>
            </div>
        </div>
    </div>
</harvestAllocation>