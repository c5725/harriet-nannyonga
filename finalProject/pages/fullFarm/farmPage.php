<?php
include("../../includes/navbar.php"); 
include("../../includes/Farm.php"); 

// links Farm to Planting
$farm = Farm::getFarmById($conn, $_GET['FarmId']);
$farm->setPlanting();
$farm->setHarvestAllocation();
 
// Convert the variables whose data type is Bool back to rendering true or false instead of 1, 0  
$isOwnedByFarmer = '';
  if ($farm->OwnedByFarmer){
    $isOwnedByFarmer ='Yes';
  } else {
    $isOwnedByFarmer = 'No';
  }

$isAvailableForSeason= '';
if ($farm->IsAvailableForSeason){
  $isAvailableForSeason ='Yes';
} else {
  $isAvailableForSeason = 'No';
}

 ?>
<!-- adding images to the masthead. images in masthead are in form of background images -->
<header class="masthead"
    <?php if (!empty($farm->primaryImage)) { ?>
        style="background-image:url(data:image/jpeg;base64,<?php echo base64_encode( $farm->primaryImage )?>)"
    <?php
        }
    ?>
>
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="post-heading">
                    <h1><?php echo "Explore " . $farm->FarmName . " Farm";?></h1>
                    <span class="meta">
						<p><?php echo " Farm Name	: " . $farm->FarmName . " Farm"?></p>
						<p> <?php echo "Farm Size: " . $farm->FarmSize . "acres"; ?> </p>
						<p> <?php echo "District Location: " .  $farm->District; ?> </p>
                    </span>
                </div>
            </div>
        </div>
    </div>
</header>
 
<farm class="mb-4">
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">	
				<p> <?php echo "Does Farmer Own Farm?: " . $isOwnedByFarmer; ?> </p> <!-- Boolean converted back to Yes or No -->							
				<p> <?php echo "Farm Available for this Season? : " . $isAvailableForSeason; ?> </p> <!-- Boolean converted back to Yes or No -->
				<p> <?php echo "Farm Review : " . $farm->FarmReview; ?> </p>
            </div>
        </div>
    </div>

</farm>

<!--  Handling the Crops for each individual Farm.  Harriet, this is you attempt to link in All crops. Not sure it works --> 
<div class="container"> 
	<div class="row">
		<?php
			if(!empty($farm->AllCrops)) {
				foreach ($farm->AllCrops as $crop) {
				$crop->CropName(); 
		?>
		<div class="col-12 col-md-4"> 
				<a class="card-wrapper"
					href="./cropPage.php?CropId=<?php echo $crop->CropId ?>">
				<div class="card"> 
				<!-- <?php print_r($crop); ?>     used to test content of $crop --> 
				<h2><?php echo "Crop Details on: " . $farm->FarmName . " Farm"?></h2>
					<p> <?php echo "Crop Name: " . $crop->CropName; ?> </p>	
			  		<p> <?php echo "Growth Period: " .  $crop->GrowthPeriod; ?> </p>
			  		<p> <?php echo "Is Likestock Fodder Component : " .  $isLivestockFodderComponent; ?> </p> <!-- Boolean converted back to Yes  or NO -->				
					<p> <?php echo "Is Indigenous : " .  $isIndigenous; ?> </p>	<!-- Boolean converted back to Yes  or NO -->
				</div> 
				</a>
		</div> 
			<?php
			}
		}
			?>
	</div>
</div> 

<!-- Handling the Plantings for each individual Farm. This code for Plantings was working mainly fine -->
<div class="container">
	<div class="row">
		<?php
			if(!empty($farm->AllPlantings)) {
				foreach ($farm->AllPlantings as $planting) {
				$planting->calculateTotalCostPerPlanting();
		?>
		<div class="col-12 col-md-4">
			<a class="card-wrapper"
				href="./plantingPage.php?PlantingId=<?php echo $planting->PlantingId ?>">
			<div class="card">
				<h2><?php echo "Planting Decisions on: " . $farm->FarmName . " Farm"?></h2>
						<p> <?php echo "Acreage Planted: " . $planting->AcreagePlanted; ?> </p>
						<p> <?php echo "Total Cost: " . $planting->TotalCostPerPlanting; ?> </p>
			</div>
			</a>
		</div>
			<?php
		}
      }
	?>
  </div>
</div>

<!-- Handling the Harvest Allocations for each individual Farm. This code for HarvestAllocations was working mainly fine -->
<div class="container">
	<div class="row">
			<?php
			if(!empty($farm->AllHarvestAllocations)) {
				foreach ($farm->AllHarvestAllocations as $harvestAllocation) {
				$harvestAllocation->calculateTotalAmountLostPostHarvest();
				$harvestAllocation->calculateCropAmountAvailableForMarket();
				$harvestAllocation->calculateRevenueFromHarvest();
			//	$harvestAllocation-> calculateProfitFromHarvest(); 
			?>
		<div class="col-12 col-md-4">
				<a class="card-wrapper"
				href="./HarvestAllocationPage.php?HarvestAllocationId=<?php echo $harvestAllocation->HarvestAllocationId ?>">
			<div class="card">
				<!-- <?php print_r($harvestAllocation); ?> -->
				<h2><?php echo "Harvest Allocations Decisions on: " . $farm->FarmName . " Farm"?></h2>
					<p> <?php echo "Actual Yield : " . $harvestAllocation->ActualYield ?> </p>
					<p> <?php echo "Total Amount Lost Post Harvest: " . $harvestAllocation->TotalAmountLostPostHarvest; ?> </p>
					<p> <?php echo "Crop Amount Available For Market: " . $harvestAllocation->CropAmountAvailableForMarket; ?> </p>
					<p> <?php echo "Market Price of Harvest  : " . $harvestAllocation->MarketPriceOfHarvest; ?> </p>
					<p> <?php echo "Revenue From Harvest: " . $harvestAllocation->RevenueFromHarvest; ?> </p>
					<p> <?php echo "Profit From Harvest: " .  $harvestAllocation->RevenueFromHarvest - $planting->TotalCostPerPlanting; ?> </p>
			</div>
				</a>
		</div>
			<?php
			}
			}
      
			?>
	</div>
</div>
