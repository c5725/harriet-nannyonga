<?php
include("../../includes/navbar.php"); //Brings in the Navbar

include("../../includes/Crop.php"); 
// brings in the crop 
$crop = Crop::getCropById($conn, $_GET['CropId']);

// Convert the variables whose data type is Bool back to rendering true or false instead of 1, 0  
$isIndigenous = '';
  if ($crop->IsIndigenous){
    $isIndigenous ='Yes';
  } else {
    $isIndigenous = 'No';
  }

$isLivestockFodderComponent= '';
if ($crop->IsLivestockFodderComponent){
  $isLivestockFodderComponent ='Yes';
} else {
  $isLivestockFodderComponent = 'No';
}

?>
 
<header class="masthead">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="post-heading">
                    <h1><?php echo "Crops Grown on Farm";?></h1>
                    <span class="meta">
						<p> <?php echo "Name of Crop Planted : " .  $crop->CropName; ?> </p>
                    </span>
                </div>
            </div>
        </div>
    </div>
</header>
 
<crop class="mb-4">
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
				<h2><?php echo "Crops Characteristics on Farm"?></h2>
			  		<p> <?php echo "Growth Period: " .  $crop->GrowthPeriod; ?> </p>
			  		<p> <?php echo "Is Likestock Fodder Component : " .  $isLivestockFodderComponent;?> </p>  
					<p> <?php echo "Is Indigenous : " . $isIndigenous; ?> </p>			   
            </div>
        </div>
    </div>
</crop>