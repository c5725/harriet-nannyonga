<?php
  include("../includes/navbar.php");
 
  if (isset($_SESSION['userName'])) {  // checks if session is set and if yes, it displays a simple splash page but if not he it links to login page
?>                                      
 
<div class='container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h1>Hello <?php echo $_SESSION['userName'] ?></h1>
            <a class="btn btn-primary" href="FarmListing.php">View/Edit 
        Farms</a>
        </div>
    </div>
</div>
 
<?php
} else {
?>
   
<a href="login.php">Please log in to see this page</a>
 
<?php
}

?>