<?php 
include("./..//includes/navbar.php");
?>
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Justifying the Onfarm Post-Harvest Calculator </h1>
    <p class="lead"> The idea for creating an onfarm post-harvest calculator was born out of observations of the food-income choices made by agricultural households in Uganda. Referred to as smallholder farmers, these individuals or families operate small farming units of an average of 2.4 acres (according to the United Nations’ Food and Agriculture Organization (FAO), which serve as the principal source of food and income. Each farming decision represents an inner debate of whether it is worthy investing in the farm and how much of the output should be made available to the market to recover these costs.  </p>
    <p class="lead"> One of the key problems smallholder farmers face is the loss of a significant amount of the mature output just before, during, and after harvest (collectively referred to as post-harvest loss). The problem is worsened because most smallholder farmers don’t keep proper records of how much is invested in growing the crops and how much of the product is harvested and/or allocated to different purposes (eaten in households, sold, gifted, stored, used for replanting). Furthermore, farmers have limited access to storage or processing facilities. Much of the food spoils in the garden or as they search for a favorable market. The overall impact is to reduce the financial returns farmers earn from operating their farms and increase in food insecurity.  </p>

    <h2 class="display-6"> Potential of the Onfarm Post-Harvest Calculator </h2>
    <p> Operators of large commercial farms primarily make choices very similar to for-profit enterprises. As a result, the market has made available different tools available to them to use to accomplish their goals. The OFPHC was designed to meet the special circumstances of smallholder farmers whose food security and profit-making status are strongly linked to decisions they make on daily basis as they operate the farm. With this tool, one is able to get a glimpse into what different levels of investments and allocations of the harvest would mean to a farmer who largely relies on the farm output to feed their family. It is easy and fast to use on different platforms.  </p>
  </div>
</div>
</body>		