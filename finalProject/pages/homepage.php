<?php
include("../includes/navbar.php");
include("../includes/Farm.php"); 
?>
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Welcome to Onfarm Post-Harvest Calculator </h1>
    <p class="lead">The Onfarm Post-harvest Calculator (OFPHC) is a tool which users, especially small farmers, can use to track the outcomes of planting crops, harvest use and storage, and sales decisions on each farm. By using the OFPHC, a current or potential farmer can determine the different levels of inputs, the costs and method of production, how to allocate the resulting harvest, and calculate potential contribution to storage the profits to be made. Through this process, the user can begin to identify and contact preferred potential storage or processing facilities to purchase their produce and contribution to promoting food security. </p>
 
    <h2 class="display-6">Features of the Onfarm Post-Harvest Calculator</h2>
    <p> Friend, glad you are here! Do you ever wonder how you can invest in producing crops, face multiple challenges leading to the loss of the farm’s output, but still have enough to eat and sale at a profit? Well, be sure to try using this tool, the Onfarm Post-Harvest Calculator (OFPHC). You will not regret exploring the OFPHC? By proving information on a few selected decisions on the farm, and anticipating the inevitable interactions with the vagaries of nature, the OFPHC can quickly and accurately provide you with a snapshot of current and future farming operations. </p>
  </div>
</div>
<?php
include("../includes/3x3Cards.php"); 
include("../includes/carousel.php");
?>
<footer>
<p>Author: Harriet Nannyonga<br>
</p>
</footer>
</body>		