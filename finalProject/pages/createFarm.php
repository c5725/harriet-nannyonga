
<?php
include("../includes/navbar.php");

$imageErr = "";            // part of element for photo uploads

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $mbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
  if ($mbFileSize > 10) {
      $imageErr = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize MB";
  }
    $FarmName       = clean_input($_POST["FarmName"]);
    $FarmSize       = clean_input($_POST["FarmSize"]);
    $District       = clean_input($_POST["District"]);
    $OwnedByFarmer  = clean_input($_POST["OwnedByFarmer"]);
    //$IsAvailableForSeason = clean_input($_POST["IsAvailableForSeason"]);
    $FarmReview     = clean_input($_POST["FarmReview"]);

    $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);  
    $imageTitle = htmlspecialchars($_FILES["fileToUpload"]["name"]);

    $IsAvailableForSeason = 0;
    if (isset($_POST['AcquireFarm'])) {
      $IsAvailableForSeason = 1;
    }

    if (!empty($FarmName) && !empty($FarmSize) && !empty($District)  && !empty($OwnedByFarmer)  && !empty($FarmReview)) {
      $UserId = getUserId($conn, $_SESSION['userName']);
      
      $farmInfo = array(
        "FarmId " => "",
        "FarmName" => $FarmName,
        "FarmSize" => $FarmSize,
        "District" => $District,
        "OwnedByFarmer" => $OwnedByFarmer,
        "IsAvailableForSeason" => $IsAvailableForSeason,  
        "FarmReview" => $FarmReview,
        "fullName" => $UserId,
        "primaryImage" => $primaryImage,
        "imageTitle" => $imageTitle

      );
   
      $farm = new Farm($conn, $farmInfo);
      $farm->createFarm(); 
       header("Location: FarmListing.php");
    }
  }

?>
 
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
      <form  enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="form-group">
          <label for="FarmName">Farm Name</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="FarmName" id="FarmName" required>
        </div>
        <div class="form-group">       <!-- input type file will open to a file select window -->
            <label for="fileToUpload">Select image to upload:</label>
            <input type="file" name="fileToUpload" id="fileToUpload" required>
            <span class="error">* <?php echo $imageErr;?></span><br>
        </div>
        <div class="form-group">
          <label for="FarmSize">Farm Size </label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="FarmSize" id="FarmSize" value="<?php echo $farm->FarmSize ?>"required>
        </div>
        <div class="form-group">
          <label for="District">District</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="District" id="District" value="<?php echo $farm->District ?>"required>
        </div>
        <div class="form-group">
          <label for="OwnedByFarmer">Owned By Farmer</label>
          <input type="checkbox" id="OwnedByFarmer" name="OwnedByFarmer" <?php echo ($farm->OwnedByFarmer ? "checked" : "") ?>>
        </div>	
        <div class="form-group">
          <label for="IsAvailableForSeason">IsAvailableForSeason </label>
          <input type="checkbox" id="IsAvailableForSeason" name="IsAvailableForSeason" <?php echo ($farm->IsAvailableForSeason  ? "checked" : "") ?>>
        </div>
        <div class="form-group">
          <label for="FarmReview">Farm Review</label>
          <span class="error">*<br>
          <textarea rows="10" class="form-control" name="FarmReview" id="FarmReview" required></textarea>
        </div>
        <div class="form-group">
          <label for="AcquireFarm">Acquire Farm</label>
          <input type="checkbox" id="AcquireFarm" name="AcquireFarm">
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>    
    </div>
  </div>
</div>