<?php
include("../includes/navbar.php");
 
if (isset($_GET['editFarmId'])) {
  try {
    $farm = Farm::getFarmById($conn, $_GET['editFarmId']);
  } catch(Exception) {
    header("Location: FarmListing.php");
  }
} else {
  header("Location: FarmListing.php");
}

// User gets option to edit the image
$imageErr = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $mbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
  if ($mbFileSize > 10) {
    $imageErr = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize MB";
  }

  // $fullName       = clean_input($_POST["fullName"]); updating the full Name is disabled or postponed
  $FarmName	      = clean_input($_POST["FarmName"]);
  $FarmSize       = clean_input($_POST["FarmSize"]);
  $District       = clean_input($_POST["District"]);
  $OwnedByFarmer  = clean_input($_POST["OwnedByFarmer"]);
  $FarmReview     = clean_input($_POST["FarmReview"]);
  $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);
  $imageTitle = htmlspecialchars($_FILES["fileToUpload"]["name"]);
  

$IsAvailableForSeason = 0;
if (isset($_POST['AquireFarm'])) {
  $IsAvailableForSeason = 1;
}

  if (!empty($FarmName) && !empty($FarmSize) && !empty($District) && !empty($OwnedByFarmer) && !empty($FarmReview)) {
	// Here is the process of updating the Farm; updating full name is postponed/disabled
  $farm->FarmName             = $FarmName;
  $farm->FarmSize             = $FarmSize;
  $farm->District             = $District;
  $farm->OwnedByFarmer        = $OwnedByFarmer;
  $farm->IsAvailableForSeason = $IsAvailableForSeason;
  $farm->FarmReview           = $FarmReview;
  //how do you handle the CropId which is the foreign key for Farms
  
  $farm->primaryImage = $primaryImage;
  $farm->imageTitle = $imageTitle;
  $farm->updateFarm();
  header("Location: FarmListing.php");

  }
}

?>
 
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
	
      <form enctype="multipart/form-data" method="post" action="<?php 
      htmlspecialchars($_SERVER["PHP_SELF"]);?>"> <!-- enctype takes care of image -->

        <div class="form-group">
          <label for="fullName">Full Name </label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="fullName" id="fullName" disabled value="<?php echo $farm->fullName ?>" required>
        </div>
		<div class="form-group">
          <label for="FarmName">Farm Name </label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="FarmName" id="FarmName" value="<?php echo $farm->FarmName ?>" required>
        </div>
      <!-- thumbnail to enable keeping track of the image -->
        <div class="row">
  <div class="col-9 col-md-6">
    <div class="form-group">
        <label for="fileToUpload">Select image to upload:</label>
        <input type="file" name="fileToUpload" id="fileToUpload">
        <span class="error">* <?php echo $imageErr;?></span><br>
    </div>
  </div>
  <div class="col-3 col-md-6">
    <?php if (!empty($farm->primaryImage)) { ?>
      <img class="d-block w-100" src='data:image/jpeg;base64,<?php echo base64_encode( $farm->primaryImage )?>' />
      alt="<?php echo $farm->imageTitle;?>"
    <?php } ?>
  </div>
</div>
 
		<div class="form-group">
          <label for="FarmSize">Farm Size </label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="FarmSize" id="FarmSize" value="<?php echo $farm->FarmSize ?>"required>
        </div>
        <div class="form-group">
          <label for="District">District</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="District" id="District" value="<?php echo $farm->District ?>"required>
        </div>
		<div class="form-group">
          <label for="OwnedByFarmer">Owned By Farmer</label>
          <input type="checkbox" id="OwnedByFarmer" name="OwnedByFarmer" <?php echo ($farm->OwnedByFarmer ? "checked" : "") ?>>
        </div>		
        <div class="form-group">
          <label for="IsAvailableForSeason">IsAvailableForSeason </label>
          <input type="checkbox" id="IsAvailableForSeason" name="IsAvailableForSeason" <?php echo ($farm->IsAvailableForSeason  ? "checked" : "") ?>>
        </div>
		
    <div class="form-group"> <!-- caters for the image -->
            <label for="fileToUpload">Select image to upload:</label>
            <input type="file" name="fileToUpload" id="fileToUpload">
            <span class="error">* <?php echo $imageErr;?></span><br>
        </div>
		<div class="form-group">
          <label for="FarmReview">Farm Review</label>
          <span class="error">*</span><br>
          <textarea rows="10" class="form-control" name="FarmReview" id="FarmReview" required> <?php echo $FarmReview;?> </textarea>
        </div>
			
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>  	
    </div> <!-- for the class="col-md-10  -->
  </div> <!-- for the class ="row  justify-content-center-->
</div> <!-- for the class = container -->


