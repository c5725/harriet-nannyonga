<?php
include("../includes/navbar.php");
include("../includes/Farm.php");
// $Farms = Farm::getFarmsFromDb($conn, 50, $UserId, $onlyAvailableForSeason=true); trial format
// $Farms = Farm::getFarmsFromDb($conn, 50, $UserId, $CropId, false); // original format
$Farms = Farm::getFarmsFromDb($conn, 50, false);
// Checking if user is logged in 
if (!isset($_SESSION['userName'])) {
  header("Location: 404.php");
}
?>
<div class="container">

<div class="row">
    <div class="d-flex justify-content-center">
      <a class='btn btn-success' href='createFarm.php'>Create New Farm</a>
    </div>
  </div>

  <?php
      foreach ($Farms as $Farm) {
	      $FarmId = $Farm->FarmId;
  ?>
    <div class="row">
      <div class="col-12 listing-wrapper"   <?php echo ($Farm->IsAvailableForSeason ? '' : 'unavailableForSeason') ?>">
          <div class="row">
            <div class="col-12 col-md-7">
              <a class="" href="farmPage.php?FarmId=<?php echo $FarmId ?>">
                <span><?php echo $Farm->FarmName ?></span>
              </a>
            </div>
            <div class="col-12 col-md-5 text-end">
              <a class='btn btn-success' href='editFarm.php?editFarmId=<?php echo $FarmId ?>'>Edit</a>
              <a class='btn btn-danger' href='deleteFarm.php?deleteFarmId=<?php echo $FarmId ?>'>Delete</a>
            </div>
          </div>
      </div>
    </div>
  <?php
    }
  ?>
</div>
