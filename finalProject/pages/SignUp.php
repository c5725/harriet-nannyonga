<?php

include("../includes/navbar.php"); // linking to the navbar

// Inserting information into database using prepared statements
// names should match the info in the database SQL

$fullName = $userName = $userPassword = ""; 
$passwordErr = $userNameErr = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $fullName = clean_input($_POST["fullName"]);     
    $userName = clean_input($_POST["userName"]);   
    $password1 = clean_input($_POST["password1"]);
    $password2 = clean_input($_POST["password2"]);
 
    if ($password1 !== $password2) {
      $userPassword = "";                          
      $passwordErr = "Passwords must match";
    } else {
      $userPassword = password_hash($password1, PASSWORD_DEFAULT);
    }
 
    $userNameErr = checkUsernameIsUnique($userName);
    if (empty($passwordErr) && empty($usernameErr)) {
      addUser($fullName, $userName, $userPassword);
      $_SESSION['userName'] = $userName;                // saves the info from session into superglobal variable 
      header("Location:splash.php");                   // redirects user into the splash page 
    }
}

// Checking if the user name is not yet taken i.e. it is unique
function checkUsernameIsUnique($useName) {
    $conn = connect_to_db("finalProjectHarrietNannyonga");
    $selectUser = "SELECT userName FROM Users WHERE userName=:userName";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':userName', $userName);
    $stmt->execute();
   
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return empty($stmt->fetchAll()) ? "" : "Username is already taken";
  }
  

function addUser($fullName, $userName, $userPassword) {
    $conn = connect_to_db("finalProjectHarrietNannyonga");
    $insert = "INSERT INTO Users (fullName, userName, userPassword)
    VALUES (:fullName, :userName, :userPassword)";
    $stmt = $conn->prepare($insert);
    $stmt->bindParam(':fullName', $fullName);
    $stmt->bindParam(':userName', $userName);
    $stmt->bindParam(':userPassword', $userPassword);
    $stmt->execute();
}

?>

<style>
    .error {color: #FF0000;}
</style>
<div class='userLoginForm container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="form-group">
                    <label for="fullName">Full Name</label>
                    <input type="text" class="form-control" name="userName" id="userName">
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <span class="error">* <?php echo $userNameErr;?></span><br>
                    <input type="text" class="form-control" name="username" id="username" required>
                </div>
                <div class="form-group">
                    <label for="password1">Password</label>
                    <span class="error">* <?php echo $passwordErr;?></span><br>
                    <input type="password" class="form-control" name="password1" id="password1" required>
                </div>
                <div class="form-group">
                    <label for="password2">Repeat Password</label>
                    <span class="error">* <?php echo $passwordErr;?></span><br>
                    <input type="password" class="form-control" name="password2" id="password2" required>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            </form>
        </div>
    </div>
</div>







