<?php
include("../includes/navbar.php");

// Checking to see if the user exists with a matching username to the one entered in the form

$fullName = $userName = $userPassword = "";
$passwordErr = $userNameErr = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $userName = clean_input($_POST["userName"]);
  $userPassword = clean_input($_POST["userPassword"]);
 
  if (!empty($userName) && !empty($userPassword)) {
    $userNameErr = verifyUser($userName);
    $passwordErr = verifyPassword($userName, $userPassword);

    if (empty($userNameErr) && empty($passwordErr)) {
        login($userName);  // checking that there are no errors & log the user in
      }
  
    
  }
}

 // Checking that the password matches the one in the database
function verifyPassword($userName, $userPassword) {
    $conn = connect_to_db("finalProjectHarrietNannyonga");
    $selectUser = "SELECT userName, userPassword FROM Users WHERE userName=:userName";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':userName', $userName);
    $stmt->execute();
   
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
      $hashedPassword = $listRow['userPassword'];
    if (!password_verify($userPassword, $hashedPassword)) {
      return "Incorrect password";
    }
    // comparing the hashed password with the password we got from user via the form
    if (!password_verify($userPassword, $hashedPassword)) {
        return "Incorrect password";
      }
  
    }
  }
  
// Selects a user from the database so we can check if the user exits with matching user name

function verifyUser($userName) {
  $conn = connect_to_db("finalProjectHarrietNannyonga");
  $selectUser = "SELECT userName FROM Users WHERE userName=:userName";
  $stmt = $conn->prepare($selectUser);
  $stmt->bindParam(':userName', $userName);
  $stmt->execute();
 
  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  return empty($stmt->fetchAll()) ? "userName does not exist" : "";
}
?>


<style>
    .error {color: #FF0000;}
</style>
<div class='userLoginForm container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="form-group">
                    <label for="userName">User Name</label>
                    <span class="error">* <?php echo $userNameErr;?></span><br>
                    <input type="text" class="form-control" name="userName" id="userName" required>
                </div>
                <div class="form-group">
                    <label for="password1">Password</label>
                    <span class="error">* <?php echo $passwordErr;?></span><br>
                    <input type="password" class="form-control" name="userPassword" id="password" required>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            </form>
        </div>
    </div>
</div>



