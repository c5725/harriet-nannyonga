<?php
include("../includes/navbar.php");
 
$FarmId = $_GET['deleteFarmId'];
 
if (isset($FarmId)) {
  try {
    $Farm = Farm::getFarmById($conn, $FarmId);
  } catch(Exception) {
    header("Location: FarmListing.php");
  }
} else {
  header("Location: FarmListing.php");
}
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $Farm->deleteFarm();
  header("Location: FarmListing.php");
}
?>
 
<div class="container">
  <div class="row justify-content-center text-center">
    <div class="col-md-10 col-lg-8">
      <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <label for="submit">Are you sure you want to delete this Farm?</label>
        <input type="submit" class="btn btn-danger" value="Delete">
      </form>
    </div>
    <div class="col-md-10 col-lg-8">
      <a href="FarmListing.php" class="btn btn-success">Cancel</a>
    </div>
  </div>
</div>

