<?php
echo "Harriet Watching Soccer Hobby";

//  Watching soccer is one of my hobbies

$myHobby = "Watching Soccer"; 	// Variable is a string. My hobby		
$FavoriteSoccerTeamName = "Cranes"; 	// variable is a string. Favorite soccer team is “Cranes” 		
$MaxTeamMembers = 23; // variable is an integer. Maximum number of players on the soccer team
$MaxPlayersOnField = 11; // variable is an integer. Number of Player on the field during match
$HeathyPlayerFielded = true; // variable is a Boolean. Healthy or has no injury and is to be fielded on the players’ team
$GoalsPlayerScored = 2; // variable is an integer. Number of goals scored in a game by this player in this game



//Creating the if/elseif/else statements

// Texting the ifelseifelse condition when x is set to 3


$x = 3;
if ($x % 3 == 0) {
  echo "divisible by 3";

} elseif ($x % 5 == 0) {
  echo "divisible by 5";
  
} elseif ($x % 3 == 0  &&  $x %5 == 0) {
  echo "divisible by both 3 & 5";

} else {
  echo "not divisible by 3 or 5";
}


// Texting the ifelseifelse condition when x is set to 5


$x = 5;
if ($x % 3 == 0) {
  echo "divisible by 3";

} elseif ($x % 5 == 0) {
  echo "divisible by 5";

} elseif ($x % 3 == 0  &&  $x %5 == 0) {
  echo "divisible by both 3 & 5";

} else {
  echo "not divisible by 3 or 5";
}


// Texting the ifelseifelse condition when x is set to 9


$x = 9;
if ($x % 3 == 0) {
  echo "divisible by 3";

} elseif ($x % 5 == 0) {
  echo "divisible by 5";

} elseif ($x % 3 == 0  &&  $x %5 == 0) {
  echo "divisible by both 3 & 5";

} else {
  echo "not divisible by 3 or 5";
}


// Texting the ifelseifelse condition when x is set to 10


$x = 10;
if ($x % 3 == 0) {
  echo "divisible by 3";

} elseif ($x % 5 == 0) {
  echo "divisible by 5";

} elseif ($x % 3 == 0  &&  $x %5 == 0) {
  echo "divisible by both 3 & 5";

} else {
  echo "not divisible by 3 or 5";
}



// Texting the ifelseifelse condition when x is set to 15


$x = 15;
if ($x % 3 == 0) {
  echo "divisible by 3";

} elseif ($x % 5 == 0) {
  echo "divisible by 5";

} elseif ($x % 3 == 0  &&  $x %5 == 0) {
  echo "divisible by both 3 & 5";

} else {
  echo "not divisible by 3 or 5";
}


// Texting the ifelseifelse condition when x is set to 19


$x = 19;
if ($x % 3 == 0) {
  echo "divisible by 3";

} elseif ($x % 5 == 0) {
  echo "divisible by 5";

} elseif ($x % 3 == 0  &&  $x %5 == 0) {
  echo "divisible by both 3 & 5";

} else {
  echo "not divisible by 3 or 5";
}


